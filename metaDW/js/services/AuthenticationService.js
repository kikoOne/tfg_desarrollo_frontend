/**
 * Created by jguzman on 25/06/2016.
 */

angular.module('metaDW').service('AuthenticationService', function AuthenticationService($http, urlAPI) {
    return ({
        createToken: createToken,
        refreshToken: refreshToken,
        logout: logout,
        getAllRoles: getAllRoles
    });

    function createToken(credentials) {
        return $http({
            method: 'POST',
            url: urlAPI + '/auth',
            data: credentials,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function refreshToken(tokenR) {
        return $http({
            method: 'POST',
            url: urlAPI + '/refresh',
            data: tokenR,
            headers: {
                'Content-Type': 'application/json',
            }
        });
    }

    function logout(tokenR) {
        return $http({
            method: 'DELETE',
            url: urlAPI + '/auth',
            data: tokenR,
            headers: {
                'Content-Type': 'application/json',
            }
        });
    }

    function getAllRoles() {
        return $http({
            method: 'GET',
            url: urlAPI + '/roles'
        });
    }
});
