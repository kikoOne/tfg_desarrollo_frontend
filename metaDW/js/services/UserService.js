/**
 * Created by jguzman on 05/06/2016.
 */

app.service('UserService', function UserService($http, urlAPIUser) {
    return ({
        createUser: createUser, //creación de un usuario nuevo
        getUsers: getUsers, //recuperar usuarios del sistema paginados y ordenados
        getDesactivatedUsers: getDesactivatedUsers, //recuperar usuarios recuperados del sistema paginados y ordenados
        getUser: getUser, //recuperar un usuario
        updateUser: updateUser, //modificar los datos de un usuario
        changePassword: changePassword, //modificar contraseña de usuario
        deleteUser: deleteUser, //eliminar un usuario del sistema (el usuario únicamente se desactiva, no se elimina)
        findTermsFromOwner: findTermsFromOwner,
        findScopesFromOwner: findScopesFromOwner,
        findCategoriesFromOwner: findCategoriesFromOwner,
        activateUser: activateUser,  //activar un usuario desactivado
        addTermBookmark: addTermBookmark,   //Añadir un término a los seguidos
        getTermBookmark: getTermBookmark,   //Recuperar el marcador de un término
        getTermBookmarks: getTermBookmarks, //Recuperar los términos marcados por un usuario y paginados
        getAllTermBookmarks: getAllTermBookmarks,   //Recuperar todos los términos marcados por un usuario
        removeTermBookmark: removeTermBookmark
    });

    function createUser(user) {
        return $http({
            method: 'POST',
            url: urlAPIUser,
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getUsers(page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getDesactivatedUsers(page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/desactivated?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getUser(idUser) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/' + idUser
        });
    }

    function updateUser(idUser, user) {
        return $http({
            method: 'PUT',
            url: urlAPIUser + '/' + idUser,
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function changePassword(idUser, password, newPassword) {
        return $http({
            method: 'PUT',
            url: urlAPIUser + '/' + idUser + '/newPassword?password=' + password + '&newPassword=' + newPassword,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function deleteUser(idUser) {
        return $http({
            method: 'DELETE',
            url: urlAPIUser + '/' + idUser
        });
    }

    function findTermsFromOwner(query, types, listPage) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/terms?query=' + query + '&types=' + types + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findScopesFromOwner(query, listPage) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/scopes?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findCategoriesFromOwner(query, listPage) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/categories?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function activateUser(idUser) {
        return $http({
            method: 'PUT',
            url: urlAPIUser + '/' + idUser + '/activate'
        });
    }

    function addTermBookmark(idUser, idTerm) {
        return $http({
            method: 'POST',
            url: urlAPIUser + '/' + idUser + '/bookmarks/terms/' + idTerm
        });
    }

    function getTermBookmark(idUser, idTerm) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/' + idUser + '/bookmarks/terms/' + idTerm
        });
    }

    function getTermBookmarks(idUser, page) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/' + idUser + '/bookmarks/terms?page=' + page.number + '&size=' + page.size
        });
    }

    function getAllTermBookmarks(idUser) {
        return $http({
            method: 'GET',
            url: urlAPIUser + '/' + idUser + '/bookmarks/terms/all'
        });
    }

    function removeTermBookmark(idUser, idTerm) {
        return $http({
            method: 'DELETE',
            url: urlAPIUser + '/' + idUser + '/bookmarks/terms/' + idTerm
        });
    }
});
