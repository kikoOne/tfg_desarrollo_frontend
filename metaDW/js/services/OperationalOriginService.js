/**
 * Created by jguzman on 05/07/2016.
 */

angular.module('metaDW').service('OperationalOriginService', function ($http, urlAPIOperationalOrigins) {
    return({
        createOperationalOrigin: createOperationalOrigin,
        getOperationalOrigins: getOperationalOrigins,
        getOperationalOrigin: getOperationalOrigin,
        updateOperationalOrigin: updateOperationalOrigin,
        removeOperationalOrigin: removeOperationalOrigin
    });

    function createOperationalOrigin(operationalOrigin) {
        return $http({
            method: 'POST',
            url: urlAPIOperationalOrigins,
            data: operationalOrigin,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getOperationalOrigins(page) {
        return $http({
            method: 'GET',
            url: urlAPIOperationalOrigins + '?page=' + page.number + '&size=' + page.size
        });
    }

    function getOperationalOrigin(id) {
        return $http({
            method: 'GET',
            url: urlAPIOperationalOrigins + '/' + id
        });
    }

    function updateOperationalOrigin(id, operationalOrigin) {
        return $http({
            method: 'PUT',
            url: urlAPIOperationalOrigins + '/' + id,
            data: operationalOrigin,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function removeOperationalOrigin(id) {
        return $http({
            method: 'DELETE',
            url: urlAPIOperationalOrigins + '/' + id
        });
    }
});
