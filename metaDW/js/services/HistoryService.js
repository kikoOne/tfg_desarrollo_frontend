/**
 * Created by jguzman on 18/07/2016.
 */

angular.module('metaDW').service('HistoryService', function ($http, urlAPIHistory) {
    return ({
        getScopeHistory: getScopeHistory, //Recuperar las versiones de un ámbito
        getScopeVersion: getScopeVersion, //Recuperar una versión de un ámbito
        getCategoryHistory: getCategoryHistory, //Recuperar las versiones de una categoría
        getCategoryVersion: getCategoryVersion, //Recuperar una versión de una categoría
        getTermHistory: getTermHistory, //Recuperar una versión de una categoría
        getTermHistoryVersion: getTermHistoryVersion,   //Recuperar una versión de un término
        getDimensionHistoryVersion: getDimensionHistoryVersion, //Recuperar una versión de una dimensión
        getIndicatorHistoryVersion: getIndicatorHistoryVersion, //Recuperar una versión de un indicador
        getObjectiveHistoryVersion: getObjectiveHistoryVersion, //Recuperar una versión de un objetivo
        getInformHistoryVersion: getInformHistoryVersion    //Recuperar una versión de un informe
    });

    function getScopeHistory(idScope, page) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/scopes/' + idScope + '?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function getScopeVersion(idScope, idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/scopes/' + idScope + '/versions/' + idVersion
        });
    }

    function getCategoryHistory(idCategory, page) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/categories/' + idCategory + '?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function getCategoryVersion(idCategory, idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/categories/' + idCategory + '/versions/' + idVersion
        });
    }

    function getTermHistory(idTerm, page) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/terms/' + idTerm + '?page=' + page.page + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function getTermHistoryVersion(idTerm, idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/terms/' + idTerm + '/versions/' + idVersion
        });
    }

    function getDimensionHistoryVersion(idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/terms/' + idVersion + '/dimension'
        });
    }

    function getIndicatorHistoryVersion(idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/terms/' + idVersion + '/indicator'
        });
    }

    function getObjectiveHistoryVersion(idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/terms/' + idVersion + '/objective'
        });
    }

    function getInformHistoryVersion(idVersion) {
        return $http({
            method: 'GET',
            url: urlAPIHistory + '/terms/' + idVersion + '/inform'
        });
    }
});
