/**
 * Created by jguzman on 06/06/2016.
 */

app.service('TermService', function TermService($http, urlAPITerm) {
    return ({
        createTerm: createTerm, //crear un nuevo término
        getTerm: getTerm, //recuperar un término
        getTerms: getTerms, //Recuperar todos los términos paginados
        getExamplesFromTerm: getExamplesFromTerm, //recuperar ejemplos de un término
        getExclusionsFromTerm: getExclusionsFromTerm, //recuperar exclusiones de un término
        getDenominationsFromTerm: getDenominationsFromTerm, //recuperar denominaciones de un término
        updateTerm: updateTerm, //modificar los datos de un término
        deleteTerm: deleteTerm, //eliminar un término
        getTermTypes: getTermTypes,  //recuperar los tipos de términos
        getTermScope: getTermScope,  //recuperar el ámbito de un término
        getTermCategories: getTermCategories, //recuperar las categorías de un término
        getTermDimension: getTermDimension, //recuperar las características de término de tipo dimensión
        getTermIndicator: getTermIndicator, //recuperar las características de término de tipo indicador
        getTermObjective: getTermObjective, //recuperar las características de término de tipo objetivo
        getTermInform: getTermInform, //recuperar las características de término de tipo informe
        getTermIncidences: getTermIncidences, //Recuperar las incidencias asociadas a un término
        getAsociatedIndicatorsFromObjective: getAsociatedIndicatorsFromObjective, //Recuperar los indicadores asociads a un término
        getInformIndicators: getInformIndicators,   //Recuperar los indicadores asociados a un informe
        getDimensionIndicators: getDimensionIndicators, //Recuperar los indicadores asociados a una dimensión
        getObjectiveDimensions: getObjectiveDimensions  //Recuperar los indicadores asociados a un objetivo
    });

    function createTerm(term) {
        return $http({
            method: 'POST',
            url: urlAPITerm,
            data: term,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getTerm(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm
        });
    }

    function getTerms(page) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function getExamplesFromTerm(idTerm, page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/examples' + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getExclusionsFromTerm(idTerm, page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/exclusions' + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getDenominationsFromTerm(idTerm, page, size) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/denominations' + '?page=' + page + '&size=' + size
        });
    }

    function updateTerm(idTerm, term) {
        return $http({
            method: 'PUT',
            url: urlAPITerm + '/' + idTerm,
            data: term,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function deleteTerm(idTerm) {
        return $http({
            method: 'DELETE',
            url: urlAPITerm + '/' + idTerm
        });
    }

    function getTermTypes() {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/types'
        });
    }

    function getTermScope(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/scope'
        });
    }

    function getTermCategories(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/categories'
        });
    }

    function getTermDimension(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/dimension'
        });
    }

    function getTermIndicator(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/indicator'
        });
    }

    function getTermObjective(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/objective'
        });
    }

    function getTermInform(idTerm) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/inform'
        });
    }

    function getTermIncidences(idTerm, page, size) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/incidences?page=' + page + '&size=' + size
        });
    }

    function getAsociatedIndicatorsFromObjective(idTerm, page, size) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/objectives/asociated?page=' + page + '&size=' + size
        });
    }

    function getInformIndicators(idTerm, page, size) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/informs/indicators?page=' + page + '&size=' + size
        });
    }

    function getDimensionIndicators(idTerm, page, size) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/dimensions/indicators?page=' + page + '&size=' + size
        });
    }

    function getObjectiveDimensions(idTerm, page, size) {
        return $http({
            method: 'GET',
            url: urlAPITerm + '/' + idTerm + '/objectives/dimensions?page=' + page + '&size=' + size
        });
    }
});
