/**
 * Created by jguzman on 06/06/2016.
 */

app.service('ScopeService', function ScopeService($http, urlAPIScope) {
    return ({
        createScope: createScope, //crear un nuevo ámbito
        getScopes: getScopes, //recuperar los ámbitos registrados paginados y ordenados
        getScope: getScope, //recuperar un ámbito
        getTermsByScope: getTermsByScope, //recuperar los términos de un ámbito
        updateScope: updateScope, //actualizar los datos de un término
        deleteScope: deleteScope //eliminar un ámbito registrado
    });

    function createScope(scope) {
        return $http({
            method: 'POST',
            url: urlAPIScope,
            data: scope,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getScopes(page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPIScope + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getScope(idScope) {
        return $http({
            method: 'GET',
            url: urlAPIScope + '/' + idScope
        });
    }

    function getTermsByScope(idScope, page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPIScope + '/' + idScope + '/terms' + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function updateScope(idScope, scope) {
        return $http({
            method: 'PUT',
            url: urlAPIScope + '/' + idScope,
            data: scope,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function deleteScope(idScope) {
        return $http({
            method: 'DELETE',
            url: urlAPIScope + '/' + idScope
        });
    }
});
