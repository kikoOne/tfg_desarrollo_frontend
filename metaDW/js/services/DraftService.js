/**
 * Created by jguzman on 29/07/2016.
 */

angular.module('metaDW').service('DraftService', function ($http, urlAPIDraft) {
    return ({
        addDraftScope: addDraftScope,   // Añadir un borrador de ambito
        getDraftScopesFromUser: getDraftScopesFromUser, //Recuperar los borradores de ámbitos de un usuario
        getDraftScope: getDraftScope,   //Recuperar el borrador de un ámbito
        updateDraftScope: updateDraftScope, //Actualizar el borrador de un ámbito
        removeDraftScope: removeDraftScope, //Eliminar un borrador de un ámbito
        addDraftCategory: addDraftCategory, // Añadir un borrador de categoría
        getDraftCategoriesFromUser: getDraftCategoriesFromUser, //Recuperar los borradores de categorías de un usuario
        getDraftCategory: getDraftCategory, //Recuperar el borrador de una categoría
        updateDraftCategory: updateDraftCategory,   //Actualizar el borrador de una categoría
        removeDraftCategory: removeDraftCategory,   //Eliminar un borrador de una categoría
        addDraftTerm: addDraftTerm, // Añadir un borrador de término
        getDraftTermsFromUser: getDraftTermsFromUser,   //Recuperar los borradores de términos de un usuario
        getDraftTerm: getDraftTerm, //Recuperar el borrador de un término
        updateDraftTerm: updateDraftTerm,   //Actualizar el borrador de un término
        removeDraftTerm: removeDraftTerm,   //Eliminar un borrador de un término
        getDraftTermDimension: getDraftTermDimension,   //Recuperar el borrador de una dimensión
        getDraftTermIndicator: getDraftTermIndicator,   //Recuperar el borrador de un indicador
        getDraftTermObjective: getDraftTermObjective,   //Recuperar el borrador de un objetivo
        getDraftTermInform: getDraftTermInform  //Recuperar el borrador de un informe
    });
    
    function addDraftScope(scope) {
        return $http({
            method: 'POST',
            url: urlAPIDraft + '/scopes',
            data: scope,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getDraftScopesFromUser(page) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/scopes?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }
    
    function getDraftScope(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/scopes/' + idDraft
        });
    }

    function updateDraftScope(idDraft, scope) {
        return $http({
            method: 'PUT',
            url: urlAPIDraft + '/scopes/' + idDraft,
            data: scope,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    
    function removeDraftScope(idDraft) {
        return $http({
            method: 'DELETE',
            url: urlAPIDraft + '/scopes/' + idDraft
        });
    }
    
    function addDraftCategory(category) {
        return $http({
            method: 'POST',
            url: urlAPIDraft + '/categories',
            data: category,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    
    function getDraftCategoriesFromUser(page) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/categories?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }
    
    function getDraftCategory(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/categories/' + idDraft
        });
    }
    
    function updateDraftCategory(idDraft, category) {
        return $http({
            method: 'PUT',
            url: urlAPIDraft + '/categories/' + idDraft,
            data: category,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    
    function removeDraftCategory(idDraft) {
        return $http({
            method: 'DELETE',
            url: urlAPIDraft + '/categories/' + idDraft
        });
    }
    
    function addDraftTerm(term) {
        return $http({
            method: 'POST',
            url: urlAPIDraft + '/terms',
            data: term,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    
    function getDraftTermsFromUser(page) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/terms?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }
    
    function getDraftTerm(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/terms/' + idDraft
        });
    }

    function updateDraftTerm(idDraft, term) {
        return $http({
            method: 'PUT',
            url: urlAPIDraft + '/terms/' + idDraft,
            data: term,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    
    function removeDraftTerm(idDraft) {
        return $http({
            method: 'DELETE',
            url: urlAPIDraft + '/terms/' + idDraft
        });
    }
    
    function getDraftTermDimension(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/terms/' + idDraft + '/dimensions'
        });
    }

    function getDraftTermIndicator(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/terms/' + idDraft + '/indicators'
        });
    }

    function getDraftTermObjective(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/terms/' + idDraft + '/objectives'
        });
    }

    function getDraftTermInform(idDraft) {
        return $http({
            method: 'GET',
            url: urlAPIDraft + '/terms/' + idDraft + '/informs'
        });
    }
});
