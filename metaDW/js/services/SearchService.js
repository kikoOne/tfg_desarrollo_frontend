/**
 * Created by jguzman on 20/06/2016.
 */

app.service('SearchService', function SearchService($http, urlAPISearch, PendingRequestService, $q) {
    return ({
        findScopesByName: findScopesByName, //Recuperar los ámbitos paginados con búsqueda por nombre
        findCategoriesByName: findCategoriesByName,  //Recuperar las categorías paginadas con búsqueda por nombre
        findOperationalOriginsByName: findOperationalOriginsByName,  //Recuperar los origenes operacionales paginados cond búsqueda por nombre
        findAnalyticalApplicativesByName: findAnalyticalApplicativesByName,  //Recuperar los aplicativos analíticos paginados con búsqueda por nombre
        findTermDimensionsByName: findTermDimensionsByName, //Recuperar las dimensiones paginadas con búsqueda por nombre
        findTermObjectivesByName: findTermObjectivesByName, //Recuperar los objetivos paginados con búsqueda por nombre
        findTermIndicatorsByName: findTermIndicatorsByName, //Recuperar los indicadores asociados paginados con búsqueda por nombre
        findTermsByNameAndType: findTermsByNameAndType,  //Recuperar términos paginados con búsqueda por nombre y tipo
        findTermsByNameAndTypeFromCategory: findTermsByNameAndTypeFromCategory,  //Recuperar términos paginados con búsqueda por nombre y tipo de una categoría
        findCategoriesByNameFromCategory: findCategoriesByNameFromCategory, // Recuperar categorías paginadas con búsqueda por nombre y pertenecientes a una categoría
        findCategoriesByNameAndType: findCategoriesByNameAndType,    //Recuperar categorías paginadas con búsqueda por nombre y tipo
        findCategoriesByNameAndTypes: findCategoriesByNameAndTypes,    //Recuperar categorías paginadas con búsqueda por nombre y tipo
        findUsersByName: findUsersByName, //Recuperar usuarios paginados con búsqueda por nombre
        findDesactivatedUsersByName: findDesactivatedUsersByName,   //Recuperar usuarios desactivados paginados con búsqueda por nombre
        findTermsByNameAndTypeFromScope: findTermsByNameAndTypeFromScope,    //Recuperar los términos paginados, por nombre y tipo, de un ámbito
        findTermsByName: findTermsByName    //Recuperar los términos por nombre
    });

    function findScopesByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/scopes?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findCategoriesByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/categories?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findOperationalOriginsByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/operational_origins?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findAnalyticalApplicativesByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/analytical_applicatives?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermDimensionsByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/dimensions?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermIndicatorsByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/indicators?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermObjectivesByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/objectives?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermsByNameAndType(listPage, query, type) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/terms?query=' + query + '&types=' + type + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermsByNameAndTypeFromCategory(listPage, query, type, idCategory) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/categories/' + idCategory + '/terms' + '?query=' + query + '&types=' + type + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findCategoriesByNameFromCategory(listPage, query, idCategory) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/categories/' + idCategory + '/subcategories' + '?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findCategoriesByNameAndType(listPage, query, type) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/categories/type/' + type + '?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findCategoriesByNameAndTypes(listPage, query, types) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/categories/types?query=' + query + '&types=' + types + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findUsersByName(listPage, query) {
        var url = urlAPISearch + '/users?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction;
        var canceller = $q.defer();

        PendingRequestService.add({
            url: url,
            canceller: canceller
        });
        return $http({
            method: 'GET',
            url: url,
            timeout: canceller.promise
        });
    }

    function findDesactivatedUsersByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/users/desactivated?query=' + query + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermsByNameAndTypeFromScope(listPage, query, types, idScope) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/scopes/' + idScope + '/terms?query=' + query + '&types=' + types + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }

    function findTermsByName(listPage, query) {
        return $http({
            method: 'GET',
            url: urlAPISearch + '/terms?query=' + query + '&types=' + [1, 2, 3, 4, 5, 6] + '&page=' + listPage.number + '&size=' + listPage.size + '&sort=' + listPage.sort.property + ',' + listPage.sort.direction
        });
    }
});