/**
 * Created by jguzman on 06/06/2016.
 */

app.service('CategoryService', function CategoryService($http, urlAPICategory) {
    return ({
        createCategory: createCategory, //crear una nueva categoría
        getCategory: getCategory, //recuperar una categoría
        getParentCategory: getParentCategory, //recuperar la categoría padre
        getRootCategories: getRootCategories, //recuperar las categorías raíz registradas paginadas y ordenadas
        getCategoriesFromCategory: getCategoriesFromCategory, //recuperar las subcategorías de una categoría
        getTermsByCategory: getTermsByCategory, //recuperar las términos de una categoría
        updateCategory: updateCategory, //actualizar los datos de una categoria
        deleteCategory: deleteCategory //eliminar una categoría registrada
    });

    function createCategory(category) {
        return $http({
            method: 'POST',
            url: urlAPICategory,
            data: category,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getCategory(idCategory) {
        return $http({
            method: 'GET',
            url: urlAPICategory + '/' + idCategory
        });
    }

    function getParentCategory(idCategory) {
        return $http({
            method: 'GET',
            url: urlAPICategory + '/' + idCategory + '/parent'
        });
    }

    function getRootCategories(page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPICategory + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getCategoriesFromCategory(idCategory, page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPICategory + '/' + idCategory + '/subcategories' + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function getTermsByCategory(idCategory, page, size, sort) {
        return $http({
            method: 'GET',
            url: urlAPICategory + '/' + idCategory + '/terms' + '?page=' + page + '&size=' + size + '&sort=' + sort
        });
    }

    function updateCategory(idCategory, category) {
        return $http({
            method: 'PUT',
            url: urlAPICategory + '/' + idCategory,
            data: category,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function deleteCategory(idCategory) {
        return $http({
            method: 'DELETE',
            url: urlAPICategory + '/' + idCategory
        });
    }
});
