/**
 * Created by jguzman on 04/07/2016.
 */

angular.module('metaDW').service('AnalyticalApplicativeService', function ($http, urlAPIAnalyticalApplicatives) {
    return ({
        createAnalyticalApplicative: createAnalyticalApplicative,
        getAnalyticalApplicatives: getAnalyticalApplicatives,
        getAnalyticalApplicative: getAnalyticalApplicative,
        updateAnalyticalApplicative: updateAnalyticalApplicative,
        removeAnalyticalApplicatives: removeAnalyticalApplicatives
    });

    function createAnalyticalApplicative(analyticalApplicative) {
        return $http({
            method: 'POST',
            url: urlAPIAnalyticalApplicatives,
            data: analyticalApplicative,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getAnalyticalApplicatives(page) {
        return $http({
            method: 'GET',
            url: urlAPIAnalyticalApplicatives + '?page=' + page.number + '&size=' + page.size
        });
    }

    function getAnalyticalApplicative(id) {
        return $http({
            method: 'GET',
            url: urlAPIAnalyticalApplicatives + '/' + id,
        });
    }

    function updateAnalyticalApplicative(id, analyticalApplicative) {
        return $http({
            method: 'PUT',
            url: urlAPIAnalyticalApplicatives + '/' + id,
            data: analyticalApplicative,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function removeAnalyticalApplicatives(id) {
        return $http({
            method: 'DELETE',
            url: urlAPIAnalyticalApplicatives + '/' + id,
        });
    }
});
