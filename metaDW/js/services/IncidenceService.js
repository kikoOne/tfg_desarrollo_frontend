/**
 * Created by jguzman on 20/07/2016.
 */

angular.module('metaDW').service('IncidenceService', function ($http, urlAPIIncidences) {
    return({
        createIncidence: createIncidence,   //Añadir una nueva indicencia
        getIncidences: getIncidences,    //Recupearar todos las incidencias paginadas
        getLastIncidences: getLastIncidences,   //Recuperar las últimas incidencias registradas
        getFullIncidences: getFullIncidences,   //Recupearar todos las incidencias completas y paginadas
        getIncidencesFromTerm: getIncidencesFromTerm,   //Recuperar las incidencias asociadas a un término
        updateIncidence: updateIncidence,   //Actualizar inicidencia
        removeIncidence: removeIncidence    //Eliminar inicidencia
    });

    function createIncidence(newIncidence) {
        return $http({
            method: 'POST',
            url: urlAPIIncidences,
            data: newIncidence,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function getIncidences(page) {
        return $http({
            method: 'GET',
            url: urlAPIIncidences + '?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function getLastIncidences() {
        return $http({
            method: 'GET',
            url: urlAPIIncidences + '/last'
        });
    }

    function getFullIncidences(page) {
        return $http({
            method: 'GET',
            url: urlAPIIncidences + '/full?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function getIncidencesFromTerm(idTerm, page) {
        return $http({
            method: 'GET',
            url: urlAPIIncidences + '/terms/' + idTerm + '?page=' + page.number + '&size=' + page.size + '&sort=' + page.sort.property + ',' + page.sort.direction
        });
    }

    function updateIncidence(idIncidence, newIncidence) {
        return $http({
            method: 'PUT',
            url: urlAPIIncidences + '/' + idIncidence,
            data: newIncidence,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    function removeIncidence(idIncidence, idTerm) {
        return $http({
            method: 'DELETE',
            url: urlAPIIncidences + '/' + idIncidence + '/terms/' + idTerm
        });
    }
});
