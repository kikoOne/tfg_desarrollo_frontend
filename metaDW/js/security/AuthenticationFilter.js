/**
 * Created by jguzman on 29/06/2016.
 */

angular.module('metaDW').factory('AuthenticationFilter', function (JWTHelper, tokenOffset, LocalStorageService, $q, $rootScope, $injector, urlAPI) {

    function logout(config) {
        LocalStorageService.remove('myMetaDWUser');
        JWTHelper.removeTokenA();
        JWTHelper.removeTokenR();
        $rootScope.myMetaDWUser = null;
        return $q.reject(config);
    }

    function refresh(config) {
        $injector.get("$http")
            ({
                method: 'POST',
                url: urlAPI + '/refresh',
                headers: {
                    'x-auth-token': JWTHelper.getTokenR()
                }
            })
            .success(function (data, status) {
                if (status == 200) {
                    JWTHelper.saveTokenA(data.data);
                }
            })
            .error(function (data, status) {
                logout(config); //en caso de que la petición de refresh falle se desloguea al usuario
            });
    }

    function contains(string, substring) {
        if (string.indexOf(substring) == -1)
            return false;
        return true;
    }

    return {
        request: function (config) {
            if (config.method == "POST" && contains(config.url, 'auth')) {
                //DO NOTHING
            } else if (contains(config.url, 'refresh')) {    //se añade el tokenR para realizar la petición de refresco
                config.headers['x-auth-token'] = JWTHelper.getTokenR();
            } else if (config.method != "GET" || (config.method == "GET" && (contains(config.url, 'users')
                || contains(config.url, 'incidences') || contains(config.url, 'operational_origins')
                || contains(config.url, 'analytical_applicatives') || contains(config.url, 'drafts')))) { //Peticiones para las que es necesario autenticarse
                var tokenA = JWTHelper.getTokenA();
                if (tokenA) {
                    if (JWTHelper.isTokenExpired(tokenA, tokenOffset)) {
                        if (JWTHelper.getTokenR()) {
                            refresh(config);  //en caso de que esté caducado se renueva antes de que falle
                            config.headers['x-auth-token'] = JWTHelper.getTokenA();
                        } else {
                            logout(config);   //si no existe tokenR se desloguea al usuario
                        }
                    } else {
                        config.headers['x-auth-token'] = tokenA;    //se añade el tokenA a la cabecera para realizar la petición
                    }
                } else {
                    logout(config);   //si no existe tokenA se desloguea al usuario
                }

            }
            return config;
        },
        requestError: function (config) {
            return config;
        },
        response: function (response) {
            return response || $q.when(response);
        },
        responseError: function (response) {
            if (response.status == 401) {
                if (contains(response.config.url, 'refresh') || contains(response.config.url, 'auth')) {
                    logout(response.config);    //en caso de que falle una petición refresh o autenticación se desloguea al usuario
                } else {
                    if (JWTHelper.getTokenR()) {
                        refresh(response.config);  //en caso de que esté caducado se renueva antes de que falle
                        response.config.headers['x-auth-token'] = JWTHelper.getTokenA();
                        var $http = $injector.get('$http');
                        return $http(response.config);
                    } else {
                        logout(response.config);   //si no existe tokenR se desloguea al usuario
                    }
                }
            } else {
                return $q.reject(response);
            }
            return response || $q.when(response);
        }
    };
});
