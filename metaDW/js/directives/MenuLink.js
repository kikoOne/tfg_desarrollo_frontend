/**
 * Created by jguzman on 01/07/2016.
 */

angular.module('metaDW').directive('menuLink', function () {
    return {
        scope: {
            section: '='
        },
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/partials/menuLink.tmpl.html',
        link: function (scope, element) {
            var controller = element.parent().controller();

            scope.hasLinkRole = function(page) {
                return controller.hasLinkRole(page);
            }

            scope.focusSection = function () {
                // set flag to be used later when
                // $locationChangeSuccess calls openPage()
                controller.autoFocusContent = true;
            };
        }
    };
});
