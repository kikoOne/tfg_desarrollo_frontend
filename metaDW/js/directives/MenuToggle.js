/**
 * Created by jguzman on 01/07/2016.
 */

angular.module('metaDW').directive('menuToggle', function ($timeout) {
    return {
        scope: {
            section: '='
        },
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/partials/menuToggle.tmpl.html',
        link: function (scope, element) {
            var controller = element.parent().controller();

            scope.isOpen = function () {
                return controller.isOpen(scope.section);
            };
            scope.open = function () {
                controller.open(scope.section);
            };

            var parentNode = element[0].parentNode.parentNode.parentNode;
            if (parentNode.classList.contains('parent-list-item')) {
                var heading = parentNode.querySelector('h2');
                element[0].firstChild.setAttribute('aria-describedby', heading.id);
            }
        }
    };
});
