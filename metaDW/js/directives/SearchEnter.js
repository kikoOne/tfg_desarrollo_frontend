/**
 * Created by jguzman on 05/08/2016.
 */

angular.module('metaDW').directive('searchEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.searchEnter);
                });
                event.preventDefault();
            }
        });
    };
});
