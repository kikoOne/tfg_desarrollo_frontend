/**
 * Created by jguzman on 30/07/2016.
 */

angular.module('metaDW').filter('timestampdate', function () {
    return function (val) {
        if (!val) {
            return "Hace poco";
        }
        var date = new Date(val);
        var day = date.getDay();
        if (day < 10) {
            day = '0' + day;
        }
        var month = date.getMonth();
        if (month < 10) {
            month = '0' + month;
        }
        var year = date.getFullYear();
        var _date = day + '-' + month + '-' + year;

        var seconds = Math.floor((new Date() - date) / 1000);

        var interval = Math.floor(seconds / 31536000);
        if (interval > 1) { // hace mas de un año
            return _date.toUpperCase();
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) { // hace más de un mes
            return _date.toUpperCase();
        }

        interval = Math.floor(seconds / 86400);
        if (interval > 1) { //hace más de 1 día
            if (interval > 7) { //Hace más de 1 semana
                interval = Math.floor(interval / 7);
                return "Hace " + interval + " semanas";
            } else if (interval = 7) { // Hace una semana
                return "Hace 1 semana";
            } else {
                return "Hace " + interval + " días";
            }
        } else if (interval == 1) { // hace 1 día
            return "Hace 1 día";
        }

        interval = Math.floor(seconds / 3600);
        if (interval == 1) {
            return "Hace 1 hora";
        } else if (interval > 1) {
            return "Hace " + interval + " horas";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return "Hace " + interval + " minutos";
        }
        return "Hace un momento";
    };
});
