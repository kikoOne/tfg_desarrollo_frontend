/**
 * Created by jguzman on 23/05/2016.
 */

var app = angular.module('metaDW', ['ngRoute', 'angular.filter', 'ngMaterial', 'ngMessages', 'ngAria', 'ngAnimate', 'lfNgMdFileInput', 'mdColorPicker']);

/**
 * Definición de constantes (urls de la API)
 */
var urlAPI = 'http://ec2-52-17-49-198.eu-west-1.compute.amazonaws.com:8080/metaDW/1.0/metadw';
app.constant('urlAPI', urlAPI);
app.constant('urlAPIIO', urlAPI + '/io');
app.constant('urlAPITerm', urlAPI + '/terms');
app.constant('urlAPIUser', urlAPI + '/users');
app.constant('urlAPIDraft', urlAPI + '/drafts');
app.constant('urlAPIScope', urlAPI + '/scopes');
app.constant('urlAPISearch', urlAPI + '/search');
app.constant('urlAPIHistory', urlAPI + '/history');
app.constant('urlAPICategory', urlAPI + '/categories');
app.constant('urlAPIIncidences', urlAPI + '/incidences');
app.constant('urlAPIOperationalOrigins', urlAPI + '/operational_origins');
app.constant('urlAPIAnalyticalApplicatives', urlAPI + '/analytical_applicatives');

/**
 * Definición de los segundos de margen para renovar el tokenA antes de que caduque.
 */
app.constant('tokenOffset', 300);

/**
 * Configuración de temas
 */
app.config(function ($mdThemingProvider, $provide) {

    // $mdThemingProvider.theme('input', 'default')
    //     .primaryPalette('grey');

    $mdThemingProvider.alwaysWatchTheme(true);
    $mdThemingProvider.generateThemesOnDemand(true);
    $provide.value('themeProvider', $mdThemingProvider);
});

/**
 * Ejecución de acciones al iniciar la app
 */
app.run(function ($rootScope, LocalStorageService, themeProvider, $mdTheming) {

    var myMetaDWUser = LocalStorageService.getObject('myMetaDWUser');
    if (myMetaDWUser) {
        $rootScope.myMetaDWUser = LocalStorageService.getObject('myMetaDWUser');
    } else {
        $rootScope.myMetaDWUser = null;
    }
});

/**
 * Configuración del routing
 */
app.config(function ($routeProvider, $locationProvider, $provide) {

    // $provide.constant('$MD_THEME_CSS', '/**/');

    $locationProvider.html5Mode(true);

    $routeProvider.when('/', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/initView.html',
        controller: 'InitController'
    }).when('/scopes', {    //ÁMBITOS
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/scopeListView.html',
        controller: 'ScopeListController'
    }).when('/scopes/:idScope', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/scopeView.html',
        controller: 'ScopeController'
    }).when('/categories', {    //CATEGORÍAS
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/categoryListView.html',
        controller: 'CategoryListController'
    }).when('/categories/:idCategory', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/categoryView.html',
        controller: 'CategoryController'
    }).when('/terms/:idTerm', { //TÉRMINOS
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/termView.html',
        controller: 'TermController'
    }).when('/new/scopes', {    //CREAR
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newScopeView.html',
        controller: 'NewScopeController'
    }).when('/new/categories', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newCategoryView.html',
        controller: 'NewCategoryController'
    }).when('/new/terms', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newTermView.html',
        controller: 'NewTermController'
    }).when('/search/:globalSearchQuery', {    //BUSCAR
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/searchView.html',
        controller: 'SearchController'
    }).when('/my/terms', {  //MI UNIDAD
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/unity/myTermsView.html',
        controller: 'MyTermsController'
    }).when('/my/categories', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/unity/myCategoriesView.html',
        controller: 'MyCategoriesController'
    }).when('/my/scopes', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/unity/myScopesView.html',
        controller: 'MyScopesController'
    }).when('/my/drafts', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/unity/myDraftsView.html',
        controller: 'DraftController'
    }).when('/users', { //USUARIOS
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/userListView.html',
        controller: 'UserListController'
    }).when('/applicatives', {  //APLICATIVOS
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/analyticalApplicativeListView.html',
        controller: 'AnalyticalApplicativeListController'
    }).when('/origins', {   //ORÍGENES
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/operationalOriginListView.html',
        controller: 'OperationalOriginListController'
    }).when('/incidences', {   //INCIDENCIAS
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/incidenceListView.html',
        controller: 'IncidenceListController'
    }).when('/edit/scopes/:idScope', {  //EDICIÓN
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newScopeView.html',
        controller: 'EditScopeController'
    }).when('/edit/categories/:idCategory', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newCategoryView.html',
        controller: 'EditCategoryController'
    }).when('/edit/terms/:idTerm', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newTermView.html',
        controller: 'EditTermController'
    }).when('/edit/drafts/scopes/:id', {  //EDICIÓN DE BORRADOR
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newScopeView.html',
        controller: 'EditDraftScopeController'
    }).when('/edit/drafts/categories/:id', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newCategoryView.html',
        controller: 'EditDraftCategoryController'
    }).when('/edit/drafts/terms/:id', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newTermView.html',
        controller: 'EditDraftTermController'
    }).when('/edit/scopes/:idScope/version/:idVersion', {   //EDICIÓN DESDE VERSIÓN HISTÓRICO
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newScopeView.html',
        controller: 'EditScopeController'
    }).when('/edit/categories/:idCategory/version/:idVersion', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newCategoryView.html',
        controller: 'EditCategoryController'
    }).when('/edit/terms/:idTerm/version/:idVersion', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/new/newTermView.html',
        controller: 'EditTermController'
    }).when('/history/scopes/:idScope', {   //HISTÓRICO
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/history/historyScopeView.html',
        controller: 'HistoryScopeController'
    }).when('/history/categories/:idCategory', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/history/historyCategoryView.html',
        controller: 'HistoryCategoryController'
    }).when('/history/terms/:idTerm', {
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/history/historyTermView.html',
        controller: 'HistoryTermController'
    }).when('/custom', {  //Cargar popups
        templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/partials/loginDialog.tmpl.html',
        controller: 'ThemeSelectionController'
    }).otherwise({
        redirectTo: '/'
    });
});

/**
 * Inclusión del filtro de autenticación
 */
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('AuthenticationFilter');
});
