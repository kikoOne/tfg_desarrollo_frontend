/**
 * Created by jguzman on 07/06/2016.
 */

angular.module('metaDW').controller('NewCategoryController', function ($scope, $rootScope, CategoryService, SearchService, $http, urlAPIIO, JWTHelper, DraftService, $location, $mdToast) {

    $rootScope.contentHead = 'Nueva Categoría';

    $scope.newCategory = {
        version: '1.0'
    };

    $scope.categoryQuery = '';

    $scope.listPage = { //paginado y orden por defecto
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'name',
            'direction': 'asc'
        }
    };

    $scope.findCategoriesByName = function () {
        SearchService.findCategoriesByNameAndTypes($scope.listPage, $scope.categoryQuery, [-1, 0, 1])
            .success(function (data, status) {
                if (status == 200) {
                    $scope.categories = data.content;
                }
            });
    };

    $scope.findCategoriesByName();

    $scope.selectedCategory = function (value) {
        if (value) {
            $scope.newCategory.idParent = value.idCategory;
        }
    };

    $scope.publishCategory = function () {
        $scope.createCategory(1);
    };

    $scope.saveTemporalCategory = function () {
        $scope.createCategory(0);
    };

    $scope.createCategory = function (state) {
        if ($scope.newCategory.name && $scope.newCategory.version) {
            $scope.newCategory.state = state;
            $scope.newCategory.owner = JWTHelper.getTokenId();
            if ($scope.newCategory.idParent) {
                $scope.newCategory.type = 0;
            } else {
                $scope.newCategory.type = -1;
            }
            CategoryService.createCategory($scope.newCategory)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Categoría guardada').position('bottom left').hideDelay(3000));
                        $location.path('/categories/' + data.data);
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al crear categoría').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al crear categoría').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.saveDraft = function () {
        if ($scope.newCategory.name && $scope.newCategory.version) {
            $scope.newCategory.owner = JWTHelper.getTokenId();
            if ($scope.newCategory.idParent) {
                $scope.newCategory.type = 0;
            } else {
                $scope.newCategory.type = -1;
            }
            DraftService.addDraftCategory($scope.newCategory)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    /**
     * IMPORTACIÓN DESDE ARCHIVO
     */

    $scope.importFile = function () {
        if ($scope.files && $scope.files.length > 0) {

            var formData = new FormData();
            // formData.append('file', $scope.files[0]);
            angular.forEach($scope.files, function (obj) {
                formData.append('files[]', obj.lfFile);
            });

            var uploadUrl = urlAPIIO + '/imports/8';
            $http.post(uploadUrl, formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Categorías importadas').position('bottom left').hideDelay(3000));
                        $location.path('/categories');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error en importación').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en importación').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Añadir archivo').position('bottom left').hideDelay(3000));
        }
    };
});
