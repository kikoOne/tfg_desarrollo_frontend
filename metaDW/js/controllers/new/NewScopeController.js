/**
 * Created by jguzman on 07/06/2016.
 */

angular.module('metaDW').controller('NewScopeController', function ($scope, $rootScope, ScopeService, urlAPIIO, $http, JWTHelper, DraftService, $location, $mdToast) {

    $rootScope.contentHead = 'Nuevo ámbito';

    $scope.newScope = {
        idScope: null,
        version: '1.0'
    };

    $scope.publishScope = function () {
        $scope.createScope(1);
    };

    $scope.saveTemporalScope = function () {
        $scope.createScope(0);
    };

    $scope.createScope = function (state) {
        if ($scope.newScope.name && $scope.newScope.version) {
            $scope.newScope.state = state;
            $scope.newScope.owner = JWTHelper.getTokenId();
            ScopeService.createScope($scope.newScope)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Ámbito guardado').position('bottom left').hideDelay(3000));
                        $location.path('/scopes/' + data.data);
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al crear ámbito').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al crear ámbito').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    }

    $scope.saveDraft = function () {
        if ($scope.newScope.name && $scope.newScope.version) {
            $scope.newScope.owner = JWTHelper.getTokenId();

            DraftService.addDraftScope($scope.newScope)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.importFile = function () {
        if ($scope.files && $scope.files.length > 0) {
            var formData = new FormData();
            // formData.append('file', $scope.files[0]);
            angular.forEach($scope.files, function (obj) {
                formData.append('files[]', obj.lfFile);
            });

            var uploadUrl = urlAPIIO + '/imports/7';
            $http.post(uploadUrl, formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Ámbitos importados').position('bottom left').hideDelay(3000));
                        $location.path('/scopes');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error en importación').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en importación').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Añadir archivo').position('bottom left').hideDelay(3000));
        }
    };
});
