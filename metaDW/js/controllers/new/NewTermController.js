/**
 * Created by jguzman on 11/06/2016.
 */

angular.module('metaDW').controller('NewTermController', function ($scope, $rootScope, $q, TermService, SearchService, urlAPIIO, $http, JWTHelper, DraftService, $location, $mdToast) {

    $rootScope.contentHead = 'Nuevo término';

    $scope.newTerm = {
        mainTerm: {
            version: '1.0'
        }
    };

    $scope.scopeQuery = '';
    $scope.originQuery = '';

    $scope.selectedInformIndicators = [];
    $scope.selectedDimensions = [];
    $scope.selectedIndicators = [];
    $scope.selectedCategories = [];
    $scope.denominations = [];
    $scope.exclusions = [];
    $scope.categories = [];
    $scope.examples = [];

    var pendingSearch = false;
    $scope.listPage = { //paginado y orden por defecto
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'name',
            'direction': 'asc'
        }
    };

    $scope.listOriginPage = { //paginado y orden por defecto
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'description',
            'direction': 'asc'
        }
    };

    //OBTENER TIPOS DE TÉRMINOS
    TermService.getTermTypes()
        .success(function (data, status) {
            if (status == 200) {
                $scope.termTypes = data;
            }
        });

    //ÁMBITOS
    $scope.findScopesByName = function () {
        SearchService.findScopesByName($scope.listPage, $scope.scopeQuery)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scopes = data.content;
                }
            });
    };
    $scope.findScopesByName();

    $scope.selectedScope = function () {
        if ($scope.selectedScopeItem) {
            $scope.newTerm.idScope = $scope.selectedScopeItem.idScope;
        }
    };

    //ORIGENES
    $scope.findOriginByName = function () {
        SearchService.findOperationalOriginsByName($scope.listOriginPage, $scope.originQuery)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.origins = data.content;
                }
            });
    };
    $scope.findOriginByName();

    $scope.selectedOrigin = function (value) {
        if (value) {
            $scope.newTerm.origin = value.idOrigin;
        }
    };

    //CATEGORÍAS
    $scope.findCategoriesByName = function (criteria) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findCategoriesByNameAndTypes($scope.listPage, criteria, [0, 2])
                    .success(function (data, status) {
                        if (status == 200) {
                            pendingSearch = null;
                            resolve(data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    //APLICATIVOS
    $scope.findApplicativeByName = function (query) {
        SearchService.findAnalyticalApplicativesByName($scope.listOriginPage, query)
            .then(function (response) {
                if (response.status == 200) {
                    $scope.applicatives = response.data.content;
                }
            });
    };
    $scope.findApplicativeByName('');

    $scope.selectedApplicative = function (input) {
        if (input) {
            $scope.newTerm.applicative = input.idApplicative;
        }
    };

    //BUSCAR DIMENSIONES
    $scope.findTermDimensionsByName = function (input) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findTermDimensionsByName($scope.listPage, input)
                    .then(function (response) {
                        if (response.status == 200) {
                            pendingSearch = null;
                            resolve(response.data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    //BUSCAR INDICADORES
    $scope.findTermIndicatorsByName = function (input) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findTermIndicatorsByName($scope.listPage, input)
                    .then(function (response) {
                        if (response.status == 200) {
                            pendingSearch = null;
                            resolve(response.data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    function hasAllParams() {
        if ($scope.newTerm.mainTerm.name && $scope.newTerm.mainTerm.type.type && $scope.newTerm.mainTerm.version && $scope.newTerm.mainTerm.shortName && $scope.newTerm.mainTerm.definition)
            return true;
        else
            return false;
    }

    //Publicar término
    $scope.saveFinalTerm = function () {
        $scope.createTerm(1);
    };
    //Guardar versión termporal
    $scope.saveTemporalTerm = function () {
        $scope.createTerm(0);
    };

    $scope.createTerm = function (state) {
        if (hasAllParams()) {
            $scope.newTerm.mainTerm.state = state;  //Estado final/no final
            $scope.setTermData();

            TermService.createTerm($scope.newTerm)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Término guardado').position('bottom left').hideDelay(3000));
                        $location.path('/terms/' + data.data);
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar término').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar término').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    }

    $scope.saveDraft = function () {
        if (hasAllParams()) {
            $scope.setTermData();
            DraftService.addDraftTerm($scope.newTerm)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });

        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.setTermData = function () {
        $scope.newTerm.mainTerm.owner = JWTHelper.getTokenId();
        if ($scope.selectedScopeItem) {
            $scope.newTerm.idScope = $scope.selectedScopeItem.idScope; //Ámbito
        }
        /*Categorías*/
        if ($scope.selectedCategories) {
            $scope.newTerm.categories = [];
            $scope.selectedCategories.forEach(function (item) {
                $scope.newTerm.categories.push(item.idCategory);
            });
        }
        //Denominaciones
        if ($scope.denominations && $scope.denominations.length > 0) {
            $scope.newTerm.denominations = [];
            $scope.denominations.forEach(function (entry) {
                var item = {
                    idTerm: null,
                    denomination: entry,
                    idDenomination: null
                };
                $scope.newTerm.denominations.push(item);
            });
        }
        $scope.newTerm.examples = $scope.examples;  //Ejemplos
        $scope.newTerm.exclusions = $scope.exclusions;  //Exclusions
        if ($scope.selectedOriginItem) {
            $scope.newTerm.origin = $scope.selectedOriginItem;
        }

        var type = $scope.newTerm.mainTerm.type.type;    //tipo
        //Dimensiones
        if (type == 3) {
            if ($scope.selectedDimensions) {
                $scope.newTerm.indicatorsDimensions = [];
                $scope.selectedDimensions.forEach(function (item) {
                    $scope.newTerm.indicatorsDimensions.push(item.idTerm);
                });
            }
        }

        if (type == 4) {
            if ($scope.selectedIndicators) {
                $scope.newTerm.asociatedIndicators = [];
                $scope.selectedIndicators.forEach(function (item) {
                    $scope.newTerm.asociatedIndicators.push(item.idTerm);
                });
            }

            if ($scope.selectedDimensions) {
                $scope.newTerm.objectivesDimensions = [];
                $scope.selectedDimensions.forEach(function (item) {
                    $scope.newTerm.objectivesDimensions.push({
                        dimension: item.idTerm,
                        additivity: true
                    });
                });
            }
        }

        if (type == 5) {
            if ($scope.selectedInformIndicators) {
                $scope.newTerm.informIndicators = [];
                $scope.selectedInformIndicators.forEach(function (item) {
                    $scope.newTerm.informIndicators.push(item.idTerm);
                });
            }
        }
    };

    /**
     * IMPORTACIÓN DESDE ARCHIVO
     */

    $scope.importFile = function () {
        if ($scope.files && $scope.files.length > 0 && $scope.selectedType != 0) {

            var formData = new FormData();
            // formData.append('file', $scope.files[0]);
            angular.forEach($scope.files, function (obj) {
                formData.append('files[]', obj.lfFile);
            });

            var uploadUrl = urlAPIIO + '/imports/' + $scope.selectedType;
            $http.post(uploadUrl, formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Términos importados').position('bottom left').hideDelay(3000));
                        $location.path('/my/terms');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error en importación').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en importación').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Añadir archivo').position('bottom left').hideDelay(3000));
        }
    };

    $scope.selectedType = 0;
    TermService.getTermTypes()
        .success(function (data, status) {
            if (status == 200) {
                $scope.termTypes = data;
            }
        });
});
