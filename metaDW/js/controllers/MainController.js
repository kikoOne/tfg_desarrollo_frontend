/**
 * Created by jguzman on 24/05/2016.
 */

angular.module('metaDW').controller('MainController', function ($scope, $mdSidenav, $location, $rootScope, AuthenticationService, JWTHelper, UserService, $mdDialog, LocalStorageService, $http, $mdToast, themeProvider, $mdTheming) {

    var self = this;
    var savedTitle = "";

    /**
     * Autoscroll top
     */
    $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
        $("html, #ui-view").animate({scrollTop: 0}, 200);
    });

    /**
     * Búsqueda
     * @type {boolean}
     */
    $scope.seletedSearch = false;
    $scope.gotoSearch = function () {
        if (!$scope.seletedSearch) {
            $scope.seletedSearch = true;
            // savedTitle = $rootScope.contentHead;
            // $rootScope.contentHead = "Atrás";
        }
    };

    $scope.exitSearch = function () {
        if ($scope.seletedSearch) {
            $scope.seletedSearch = false;
            // $rootScope.contentHead = savedTitle;
        }
    }

    $scope.search = function () {
        var query = $scope.globalSearchQuery;
        if (query && query.length > 2) {
            $scope.globalSearchQuery = '';
            $location.path('/search/' + query);
        } else {
            $mdToast.show($mdToast.simple().textContent('Introduce al menos 3 caracteres').position('bottom left').hideDelay(3000));
        }
    };

    /**
     * MENÚ
     * @param $mdOpenMenu
     * @param ev
     */
    this.openMenu = function ($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
    };

    /**
     * Selector de tema
     * @param ev
     */
    $scope.showThemeSelectionPopup = function (ev) {

        $mdDialog.show({
                controller: ThemeSelectionController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/themeSelectionDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {

            });
    }

    /**
     * TEMAS
     */
    $scope.initTheme = function () {
        var theme = LocalStorageService.get('metaDWTheme');
        if (theme == false || theme == null || theme == 'metaDW') { //si no hay tema o es metaDW
            setMetaDWTheme();
            themeProvider.setDefaultTheme('metaDW');
        } else {    // tema personalizado
            $rootScope.metaDWTheme = theme;
            var customThemeName = loadTheme();
            themeProvider.setDefaultTheme(customThemeName);
        }
    }

    /**
     * Se añade el tema metaDW
     */
    function setMetaDWTheme() {
        var customBlueMap = themeProvider.extendPalette('light-blue', {
            'contrastDefaultColor': 'light',
            'contrastDarkColors': ['50'],
            '50': 'ffffff'
        });
        themeProvider.definePalette('customBlue', customBlueMap);
        themeProvider.theme('metaDW')
            .primaryPalette('customBlue', {
                'default': '500',
                'hue-1': '50'
            })
            .accentPalette('red');
            // .warnPalette('red');
        themeProvider.theme('input', 'metaDW')
            .primaryPalette('grey');
        
        $mdTheming.generateTheme('metaDW');

        $rootScope.metaDWTheme = 'metaDW';
        LocalStorageService.set('metaDWTheme', 'metaDW');
    }

    /**
     * Carga el tema personalizado
     */
    function loadTheme() {

        $rootScope.metaDWTheme = LocalStorageService.get('metaDWTheme');
        $rootScope.primaryColor = LocalStorageService.get('primaryColor');
        $rootScope.accentColor = LocalStorageService.get('accentColor');
        $rootScope.backgroundColor = LocalStorageService.get('backgroundColor');

        if (!$rootScope.primaryColor || !$rootScope.accentColor || !$rootScope.backgroundColor) { //si faltan parámetros
            setMetaDWTheme();
            $rootScope.metaDWTheme = 'metaDW';
            LocalStorageService.set('metaDWTheme', 'metaDW');
        } else {
            var themeName = $rootScope.primaryColor + '_' + $rootScope.accentColor + '_' + $rootScope.backgroundColor;
            themeProvider.theme(themeName)
                .primaryPalette($rootScope.primaryColor)
                .accentPalette($rootScope.accentColor)
                .backgroundPalette($rootScope.backgroundColor);

            $mdTheming.generateTheme(themeName);
            return themeName;
        }
    }

    /**
     * Controlador de cambio de tema
     * @param $scope
     * @param $mdDialog
     * @constructor
     */
    function ThemeSelectionController($scope, $rootScope, $mdDialog, themeProvider, $mdTheming, $mdToast) {

        $scope.metaDWTheme = $rootScope.metaDWTheme;
        if ($scope.metaDWTheme == 'metaDW') {
            $scope.primaryColor = 'light-blue'
            $scope.accentColor = 'red';
            $scope.backgroundColor = 'grey';
        } else {
            $scope.metaDWTheme = 'custom';
            $scope.primaryColor = $rootScope.primaryColor;
            $scope.accentColor = $rootScope.accentColor;
            $scope.backgroundColor = $rootScope.backgroundColor;
        }

        $scope.themeList = [
            'red',
            'pink',
            'purple',
            'deep-purple',
            'indigo',
            'blue',
            'light-blue',
            'cyan',
            'teal',
            'green',
            'light-green',
            'lime',
            'yellow',
            'amber',
            'orange',
            'deep-orange',
            'brown',
            'grey',
            'blue-grey'
        ];

        function changeTheme() {
            if ($scope.metaDWTheme == 'metaDW') {
                console.log('setting metaDW...')
                setMetaDWTheme();
            } else {
                console.log('setting custom...')
                setNewTheme();
            }
            $mdToast.show($mdToast.simple().textContent('Tema actualizado').position('bottom left').hideDelay(3000));
            $mdDialog.hide();
        }

        /**
         * Añade el tema personalizado
         */
        function setNewTheme() {
            if ($scope.primaryColor && $scope.accentColor && $scope.backgroundColor) {
                var themeName = $scope.primaryColor + '_' + $scope.accentColor + '_' + $scope.backgroundColor;
                themeProvider.theme(themeName)
                    .primaryPalette($scope.primaryColor)
                    .accentPalette($scope.accentColor)
                    .backgroundPalette($scope.backgroundColor);

                $mdTheming.generateTheme(themeName);

                $rootScope.metaDWTheme = themeName;
                LocalStorageService.set('metaDWTheme', themeName);

                $rootScope.primaryColor = $scope.primaryColor;
                LocalStorageService.set('primaryColor', $scope.primaryColor);
                $rootScope.accentColor = $scope.accentColor;
                LocalStorageService.set('accentColor', $scope.accentColor);
                $rootScope.backgroundColor = $scope.backgroundColor;
                LocalStorageService.set('backgroundColor', $scope.backgroundColor);
            } else {
                $mdToast.show($mdToast.simple().textContent('Seleccionar todos los temas').position('bottom left').hideDelay(3000));
            }
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            changeTheme();
        };
    }

    /**
     * Método de apertura del popup de cambio de contraseña
     * @param ev
     */
    $scope.showNewPasswordPopup = function (ev) {
        $mdDialog.show({
                controller: NewPasswordController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/partials/newPasswordDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {

            });
    };

    /**
     * Controlador de cambio de contraseña
     * @param $scope
     * @param $mdDialog
     * @constructor
     */
    function NewPasswordController($scope, $mdDialog, UserService, $mdToast) {

        function newPassword() {
            UserService.changePassword(JWTHelper.getTokenId(), $scope.credentials.password, $scope.credentials.newPassword)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Contraseña actualizada').position('bottom left').hideDelay(3000));
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al actualizar contraseña').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al actualizar contraseña').position('bottom left').hideDelay(3000));
                });
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            newPassword();
        };
    }

    /**
     * Popup de exportación
     * @param ev
     */
    $scope.showExportPopup = function (ev) {
        $mdDialog.show({
                controller: ExportController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/partials/exportDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {

            });
    }

    /**
     * Controlador de exportación
     * @param $scope
     * @param $mdDialog
     * @param TermService
     * @param urlAPIIO
     * @constructor
     */
    function ExportController($scope, $mdDialog, TermService, urlAPIIO) {

        TermService.getTermTypes()
            .success(function (data, status) {
                if (status == 200) {
                    var tmp = data;
                    tmp[6] = {name: 'Ámbito', type: 7, description: ''};
                    tmp[7] = {name: 'Categorías', type: 8, description: ''};
                    console.log(tmp);
                    $scope.termTypes = tmp;
                }
            });

        function exportFiles() {
            if ($scope.selectedType) {
                var downloadUrl = urlAPIIO + '/exports/' + $scope.selectedType;
                $http.get(downloadUrl).success(function (response, status, headers) {
                    if (status == 200) {
                        // var type_extension = "data:text/csv;charset=UTF-8";//headers('Content-Type');
                        // console.log(type_extension);
                        // var content = "\ufeff"+ response;
                        // var blob = new Blob([content], {type: type_extension});
                        // console.log(headers);
                        // saveAs(blob, "asd.csv");
                        // var url = URL.createObjectURL(blob);
                        // var downloadLink = document.createElement("a");
                        // downloadLink.href = url;
                        // downloadLink.download = "data.csv";
                        //
                        // document.body.appendChild(downloadLink);
                        // downloadLink.click();
                        // document.body.removeChild(downloadLink);

                        var csv = "data:text/csv;charset=utf-8,\uFEFF" + response;
                        var data = encodeURI(csv);

                        var link = document.createElement('a');
                        link.setAttribute('href', data);
                        link.setAttribute('download', "c.csv");
                        link.click();
                    }
                });
            }
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            exportFiles();
        };
    }

    /**
     * Método de apertura del popup de login
     * @param ev
     */
    $scope.loading = false;
    $scope.showLoginDialog = function (ev) {
        $mdDialog.show({
                controller: LoginController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/common/partials/loginDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {

            });
    };

    /**
     * Controlador Autenticación
     * @param $scope
     * @param $mdDialog
     * @param AuthenticationService
     * @param JWTHelper
     * @constructor
     */
    function LoginController($scope, $mdDialog, AuthenticationService, JWTHelper, $mdToast) {

        $scope.loading = false;
        console.log($scope.loading);
        $scope.credentials = {};
        $scope.credentials.username = 'admin';
        $scope.credentials.password = 'admin';

        function login() {
            // TODO: sustituir por identificador de dispositivo
            $scope.credentials.device = Math.floor(Math.random() * 100000000);

            console.log($scope.credentials);

            //llamada de creación de tokenR (login)
            AuthenticationService.createToken($scope.credentials)
                .success(function (data, status) {
                    if (status == 201) {
                        if (data) {
                            JWTHelper.saveTokenR(data.data);

                            //llamada de obtención de tokenA
                            AuthenticationService.refreshToken(data.data)
                                .success(function (data, status) {
                                    if (status == 200) {
                                        JWTHelper.saveTokenA(data.data);
                                        var idUser = JWTHelper.getTokenId();
                                        getMyUser(idUser);    //se recupera el usuario
                                        $mdDialog.hide();
                                    } else {
                                        $mdToast.show($mdToast.simple().textContent('Error en Log in').position('bottom left').hideDelay(3000));
                                        $scope.loading = false;
                                    }
                                })
                                .error(function (response) {
                                    $mdToast.show($mdToast.simple().textContent('Error en Log in').position('bottom left').hideDelay(3000));
                                    $scope.loading = false;
                                });
                        }
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error en usuario o contraseña').position('bottom left').hideDelay(3000));
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en usuario o contraseña').position('bottom left').hideDelay(3000));
                    $scope.loading = false;
                })
                .catch(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en usuario o contraseña').position('bottom left').hideDelay(3000));
                    $scope.loading = false;
                });
        };

        function getMyUser(idUser) {
            UserService.getUser(idUser)
                .success(function (data, status) {
                    if (status == 200) {
                        $rootScope.myMetaDWUser = data;
                        LocalStorageService.setObject('myMetaDWUser', data);
                        $scope.loading = false;
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al recuperar usuario').position('bottom left').hideDelay(3000));
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al recuperar usuario').position('bottom left').hideDelay(3000));
                    $scope.loading = false;
                });
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            $scope.loading = true;
            login();
        };
    }

    /**
     * Log out
     */
    $scope.logout = function () {
        AuthenticationService.logout(JWTHelper.getTokenR())
            .success(function (data, status) {
                if (status == 204) {
                    console.log('logout');
                    $rootScope.myMetaDWUser = null;
                    LocalStorageService.remove('myMetaDWUser');
                    JWTHelper.removeTokenA();
                    JWTHelper.removeTokenR();
                    $location.path('/');
                }
            });
    };

    /**
     * MENÚ LATERAL
     */
    self.openedSection = null;
    self.autoFocusContent = false;
    self.isOpen = isOpen;
    self.open = open;
    self.hasLinkRole = hasLinkRole;

    //Control de apertura del menú
    $scope.toggleSidenav = function (menuId) {
        $mdSidenav(menuId).toggle();
    };

    function open(section) {
        self.openedSection = (self.openedSection == section ? null : section);
    };

    function isOpen(section) {
        return self.openedSection == section;
    };

    /*PERMISOS MENÚ*/
    $scope.hasSectionRole = function (section) {
        if (section.role) {
            var myMetaDWUser = LocalStorageService.getObject('myMetaDWUser');
            if (myMetaDWUser && containsPart(section.role, myMetaDWUser.roles)) {
                return true;
            } else
                return false;
        }
        return true;
    };

    function hasLinkRole(page) {
        if (page.role) {
            var myMetaDWUser = LocalStorageService.getObject('myMetaDWUser');
            if (myMetaDWUser && contains(page.role, myMetaDWUser.roles))
                return true;
            else
                return false;
        }
        return true;
    };

    function containsPart(sectionRoles, userRoles) {
        var flag = false;
        sectionRoles.forEach(function (sectionRole) {
            userRoles.forEach(function (userRole) {
                if ((userRole.name).indexOf(sectionRole) > -1) {
                    flag = true;
                }
            });
        });
        return flag;
    }

    function contains(sectionRoles, userRoles) {
        var flag = false;
        sectionRoles.forEach(function (sectionRole) {
            userRoles.forEach(function (userRole) {
                if (userRole.name == sectionRole) {
                    flag = true;
                }
            });
        });
        return flag;
    }

    $scope.menu = {
        sections: [
            {
                name: 'Explorar',
                role: undefined,
                type: 'block',
                pages: [
                    {
                        name: 'Por ámbitos',
                        role: undefined,
                        type: 'link',
                        link: '/scopes'
                    },
                    {
                        name: 'Por categorías',
                        role: undefined,
                        type: 'link',
                        link: '/categories'
                    }
                ]
            },
            {
                name: 'Administración',
                role: ['ROLE_ADMIN'],
                type: 'block',
                pages: [
                    {
                        name: 'Usuarios',
                        role: ['ROLE_ADMIN_USERS'],
                        type: 'link',
                        link: '/users'
                    },
                    {
                        name: 'Orígenes operacionales',
                        role: ['ROLE_ADMIN_OPERATIONALORIGIN'],
                        type: 'link',
                        link: '/origins'
                    },
                    {
                        name: 'Aplicativos analíticos',
                        role: ['ROLE_ADMIN_ANALYTICALAPPLICATIVE'],
                        type: 'link',
                        link: '/applicatives'
                    },
                    {
                        name: 'Incidencias',
                        role: ['ROLE_ADMIN_INCIDENCE'],
                        type: 'link',
                        link: '/incidences'
                    }
                ]
            },
            {
                name: 'Añadir',
                role: ['CREATION'],
                type: 'block',
                pages: [
                    {
                        name: 'Nuevo ámbito',
                        role: ['ROLE_SCOPE_CREATION'],
                        type: 'link',
                        link: '/new/scopes'
                    },
                    {
                        name: 'Nueva categoría',
                        role: ['ROLE_CATEGORY_CREATION'],
                        type: 'link',
                        link: '/new/categories'
                    },
                    {
                        name: 'Nuevo término',
                        role: ['ROLE_TERM_CREATION'],
                        type: 'link',
                        link: '/new/terms'
                    }
                ]
            },
            {
                name: 'Mi unidad',
                role: ['CREATION', 'UPDATE', 'DELETE'],
                type: 'block',
                pages: [
                    {
                        name: 'Mis borradores',
                        role: undefined,
                        type: 'link',
                        link: '/my/drafts'
                    },
                    {
                        name: 'Mis ámbitos',
                        role: ['ROLE_SCOPE_CREATION', 'ROLE_SCOPE_UPDATE', 'ROLE_SCOPE_DELETE'],
                        type: 'link',
                        link: '/my/scopes'
                    },
                    {
                        name: 'Mis categorías',
                        role: ['ROLE_CATEGORY_CREATION', 'ROLE_CATEGORY_UPDATE', 'ROLE_CATEGORY_DELETE'],
                        type: 'link',
                        link: '/my/categories'
                    },
                    {
                        name: 'Mis términos',
                        role: ['ROLE_TERM_CREATION', 'ROLE_TERM_UPDATE', 'ROLE_TERM_DELETE'],
                        type: 'link',
                        link: '/my/terms'
                    }
                ]
            }
        ]
    };
});