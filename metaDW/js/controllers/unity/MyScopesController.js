/**
 * Created by jguzman on 02/07/2016.
 */

angular.module('metaDW').controller('MyScopesController', function ($rootScope, $scope, UserService, ScopeService, $mdToast) {

    $rootScope.contentHead = 'Mis ámbitos';

    $scope.searchParam = "";
    $scope.scopeListPage = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            'name': 'Nombre',
            'value': 'name'
        },
        {
            'name': 'Fecha de creación',
            'value': 'creation'
        },
        {
            'name': 'Autor',
            'value': 'owner.name'
        }];

    $scope.getScopes = function () {
        $scope.loading = true;
        UserService.findScopesFromOwner($scope.searchParam, $scope.scopeListPage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scopes = data.content;
                    $scope.scopeListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
    };

    $scope.getScopes();

    $scope.removeScope = function (scope) {
        ScopeService.deleteScope(scope.idScope)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.getScopes();
                    $mdToast.show($mdToast.simple().textContent('Ámbito eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar ámbito').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar ámbito').position('bottom left').hideDelay(3000));
            });
    };

    $scope.$watch('scopeListPage.sort.property', function () {
        if ($scope.scopes) {
            $scope.getScopes();
        }
    });

    $scope.$watch('scopeListPage.number', function () {
        if ($scope.scopes) {
            $scope.getScopes();
        }
    });

    $scope.$watch('scopeListPage.size', function () {
        if ($scope.scopes) {
            $scope.getScopes();
        }
    });

    $scope.$watch('searchParam', function () {
        if ($scope.scopes) {
            $scope.getScopes();
        }
    });

    $scope.previousPage = function () {
        $scope.scopeListPage.number--;
    };

    $scope.nextPage = function () {
        $scope.scopeListPage.number++;
    };

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_SCOPE_UPDATE')
    };

    $scope.isErasable = function () {
        return hasRole('ROLE_SCOPE_DELETE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }
});
