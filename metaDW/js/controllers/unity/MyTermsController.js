/**
 * Created by jguzman on 02/07/2016.
 */

angular.module('metaDW').controller('MyTermsController', function ($rootScope, $scope, UserService, TermService, $mdToast) {

    $rootScope.contentHead = 'Mis términos';

    $scope.searchParam = '';
    $scope.termListPage = {
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        },
        {
            name: 'Autor',
            value: 'owner.name'
        }
    ];

    $scope.selected = [];
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };
    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };

    var flag = false;
    TermService.getTermTypes()
        .success(function (data, status) {
            if (status == 200) {
                $scope.termTypes = data;
                $scope.termTypes.forEach(function (entry) {
                    $scope.toggle(entry, $scope.selected);
                });
                $scope.findTermsFromOwner();
            }
        });

    $scope.findTermsFromOwner = function () {
        $scope.loading = true;
        var selectedTypes = [];
        $scope.selected.forEach(function (entry) {
            selectedTypes.push(entry.type);
        });
        UserService.findTermsFromOwner($scope.searchParam, selectedTypes, $scope.termListPage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.terms = data.content;
                    $scope.termListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            });
    };

    $scope.removeTerm = function (term) {
        TermService.deleteTerm(term.idTerm)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.findTermsFromOwner();
                    $mdToast.show($mdToast.simple().textContent('Término eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar término').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar término').position('bottom left').hideDelay(3000));
            });
    };

    $scope.$watch('termListPage.sort.property', function () {
        if ($scope.terms) {
            $scope.findTermsFromOwner();
        }
    });

    $scope.$watch('termListPage.number', function () {
        if ($scope.terms) {
            $scope.findTermsFromOwner();
        }
    });

    $scope.$watch('termListPage.size', function () {
        if ($scope.terms) {
            $scope.findTermsFromOwner();
        }
    });

    var flagFirst = true;
    $scope.$watchCollection('selected', function () {
        if ($scope.selected.length > 0 && !flagFirst) {
            $scope.findTermsFromOwner();
        } else if ($scope.selected.length > 0 && flagFirst) {
            flagFirst = false;
        }
    });

    $scope.$watch('searchParam', function () {
        if ($scope.terms)
            $scope.findTermsFromOwner();
    });

    $scope.previousPage = function () {
        $scope.termListPage.number--;
    };

    $scope.nextPage = function () {
        $scope.termListPage.number++;
    };

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_TERM_UPDATE')
    };

    $scope.isErasable = function () {
        return hasRole('ROLE_TERM_DELETE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }
});
