/**
 * Created by jguzman on 02/07/2016.
 */

angular.module('metaDW').controller('MyCategoriesController', function ($rootScope, $scope, UserService, CategoryService, $mdToast) {

    $rootScope.contentHead = "Mis categorías";

    $scope.searchParam = "";
    $scope.categoryListPage = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        },
        {
            name: 'Autor',
            value: 'owner.name'
        }
    ];

    $scope.getCategories = function () {
        $scope.loading = true;
        UserService.findCategoriesFromOwner($scope.searchParam, $scope.categoryListPage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.categories = data.content;
                    $scope.categoryListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
    };

    $scope.getCategories();

    $scope.removeCategory = function (category) {
        CategoryService.deleteCategory(category.idCategory)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.getCategories();
                    $mdToast.show($mdToast.simple().textContent('Categoría eliminada').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar categoría').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar categoría').position('bottom left').hideDelay(3000));
            });
    };

    $scope.$watch('categoryListPage.sort.property', function () {
        if ($scope.categories) {
            $scope.getCategories();
        }
    });

    $scope.$watch('categoryListPage.number', function () {
        if ($scope.categories) {
            $scope.getCategories();
        }
    });

    $scope.$watch('categoryListPage.size', function () {
        if ($scope.categories) {
            $scope.getCategories();
        }
    });

    $scope.$watch('searchParam', function () {
        if ($scope.categories)
            $scope.getCategories();
    });

    $scope.previousPage = function () {
        $scope.categoryListPage.number--;
    };

    $scope.nextPage = function () {
        $scope.categoryListPage.number++;
    };

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_CATEGORY_UPDATE')
    };

    $scope.isErasable = function () {
        return hasRole('ROLE_CATEGORY_DELETE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }
});
