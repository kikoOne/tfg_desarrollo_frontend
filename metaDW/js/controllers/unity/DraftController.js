/**
 * Created by jguzman on 29/07/2016.
 */

angular.module('metaDW').controller('DraftController', function ($rootScope, $scope, JWTHelper, DraftService, $mdToast, $timeout) {

    $rootScope.contentHead = 'Mis borradores';

    $scope.seletedSort = 'name';
    $scope.pageSize = 10;
    $scope.scopePage = {
        number: 0,
        size: $scope.pageSize,
        sort: {
            property: $scope.seletedSort,
            direction: 'asc'
        },
        totalPages: 1
    };
    $scope.categoryPage = {
        number: 0,
        size: $scope.pageSize,
        sort: {
            property: $scope.seletedSort,
            direction: 'asc'
        },
        totalPages: 1
    };
    $scope.termPage = {
        number: 0,
        size: $scope.pageSize,
        sort: {
            property: $scope.seletedSort,
            direction: 'asc'
        },
        totalPages: 1
    };
    $scope.sortList = [
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        }
    ];

    // Recuperar ámbitos
    $scope.findScopes = function (add) {
        if (add) {
            $scope.addingScopes = true;
        } else {
            $scope.loadingScopes = true;
            $scope.scopes = null;
        }
        DraftService.getDraftScopesFromUser($scope.scopePage)
            .success(function (data, status) {
                if (status == 200) {
                    $timeout(function () {
                        if (add) {
                            data.content.forEach(function (item) {
                                $scope.scopes.push(item);
                            });
                        } else {
                            $scope.scopes = data.content;
                            $scope.scopePage.totalPages = data.totalPages;
                        }
                        $scope.lastScopePage = data.last;
                    }, 400);
                    if (add)
                        $scope.addingScopes = false;
                    else
                        $scope.loadingScopes = false;
                } else {
                    if (add)
                        $scope.addingScopes = false;
                    else
                        $scope.loadingScopes = false;
                }
            });
    };
    $scope.findScopes(0);

    // Recuperar categorías
    $scope.findCategories = function (add) {
        $scope.loadingCategories = true;
        if (add) {
            $scope.addingCategories = true;
        } else {
            $scope.loadingCategories = true;
            $scope.categories = null;
        }
        DraftService.getDraftCategoriesFromUser($scope.categoryPage)
            .success(function (data, status) {
                if (status == 200) {
                    $timeout(function () {
                        if (add) {
                            data.content.forEach(function (item) {
                                $scope.categories.push(item);
                            });
                        } else {
                            $scope.categories = data.content;
                            $scope.categoryPage.totalPages = data.totalPages;
                        }
                        $scope.lastCategoryPage = data.last;
                    }, 400);
                    if (add)
                        $scope.addingCategories = false;
                    else
                        $scope.loadingCategories = false;
                } else {
                    if (add)
                        $scope.addingCategories = false;
                    else
                        $scope.loadingCategories = false;
                }
            });
    };
    $scope.findCategories(0);

    // Recuperar términos
    $scope.findTerms = function (add) {
        if (add) {
            $scope.addingTerms = true;
        } else {
            $scope.terms = null;
            $scope.loadingTerms = true;
        }
        DraftService.getDraftTermsFromUser($scope.termPage)
            .success(function (data, status) {
                if (status == 200) {
                    $timeout(function () {
                        if (add) {
                            data.content.forEach(function (item) {
                                $scope.terms.push(item);
                            });
                        } else {
                            $scope.terms = data.content;
                            $scope.termPage.totalPages = data.totalPages;
                        }
                        $scope.lastTermPage = data.last;
                    }, 400);

                    if (add)
                        $scope.addingTerms = false;
                    else
                        $scope.loadingTerms = false;
                } else {
                    if (add)
                        $scope.addingTerms = false;
                    else
                        $scope.loadingTerms = false;
                }
            });
    };
    $scope.findTerms(0);

    $scope.find = function (add) {
        if ($scope.scopeSelection)
            $scope.findScopes(add);
        if ($scope.categorySelection)
            $scope.findCategories(add);
        if ($scope.termSelection)
            $scope.findTerms(add);
    };

    $scope.removeScope = function (scope) {
        DraftService.removeDraftScope(scope.id)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.findScopes(0);
                    $mdToast.show($mdToast.simple().textContent('Borrador eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar borrador').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar borrador').position('bottom left').hideDelay(3000));
            });
    };
    $scope.removeCategory = function (category) {
        DraftService.removeDraftCategory(category.id)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.renewPages();
                    $scope.findCategories(0);
                    $mdToast.show($mdToast.simple().textContent('Borrador eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar borrador').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar borrador').position('bottom left').hideDelay(3000));
            });
    };
    $scope.removeTerm = function (term) {
        DraftService.removeDraftTerm(term.id)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.renewPages();
                    $scope.findTerms(0);
                    $mdToast.show($mdToast.simple().textContent('Borrador eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar borrador').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar borrador').position('bottom left').hideDelay(3000));
            });
    };
    $scope.removeTerm = function (term) {
        DraftService.removeDraftTerm(term.id)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.renewPages();
                    $scope.findTerms(0);
                }
            });
    };

    /**
     * Acciones para el selector
     */
    $scope.scopeSelection = true;
    $scope.toggleScope = function () {
        $scope.scopeSelection = !$scope.scopeSelection;

    };

    $scope.categorySelection = true;
    $scope.toggleCategory = function () {
        $scope.categorySelection = !$scope.categorySelection;
    };

    $scope.termSelection = true;
    $scope.toogleTerms = function () {
        $scope.termSelection = !$scope.termSelection;
    };

    //Watchers a las variables
    $scope.$watch('seletedSort', function () {
        $scope.scopePage.sort.property = $scope.seletedSort;
        $scope.categoryPage.sort.property = $scope.seletedSort;
        $scope.termPage.sort.property = $scope.seletedSort;
        $scope.renewPages();
        $scope.find(0);
    });
    $scope.$watch('scopeSelection', function () {
        $scope.renewPages();
        if ($scope.scopeSelection) {
            $scope.findScopes(0);
        } else {
            $scope.scopes = [];
        }
    });
    $scope.$watch('categorySelection', function () {
        $scope.renewPages();
        if ($scope.categorySelection) {
            $scope.findCategories(0);
        } else {
            $scope.categories = [];
        }
    });
    $scope.$watch('termSelection', function () {
        $scope.renewPages();
        if ($scope.termSelection) {
            $scope.findTerms(0);
        } else {
            $scope.terms = [];
        }
    });
    $scope.$watch('pageSize', function () {
        $scope.scopePage.size = $scope.pageSize;
        $scope.categoryPage.size = $scope.pageSize;
        $scope.termPage.size = $scope.pageSize;
        $scope.renewPages();
        $scope.find(0);
    });
    $scope.nextScopePage = function () {
        $scope.scopePage.number++;
        $scope.findScopes(1);
    };
    $scope.nextCategoryPage = function () {
        $scope.categoryPage.number++;
        $scope.findCategories(1);
    };
    $scope.nextTermPage = function () {
        $scope.termPage.number++;
        $scope.findTerms(1);
    };
    $scope.renewPages = function () {
        $scope.termPage.number = 0;
        $scope.categoryPage.number = 0;
        $scope.scopePage.number = 0;
    };

    $scope.mouseenter = function (term) {
        term.hover = true;
    };

    $scope.mouseleave = function (term) {
        term.hover = false;
    };
});
