/**
 * Created by jguzman on 18/07/2016.
 */

angular.module('metaDW').controller('HistoryScopeController', function ($rootScope, $scope, $routeParams, HistoryService, ScopeService) {

    $rootScope.contentHead = 'Historial de ámbito';

    $scope.scopePage = {
        number: 0,
        size: 20,
        sort: {
            property: 'lastUpdate',
            direction: 'desc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            name: 'Más antiguo',
            value: {
                property: 'lastUpdate',
                direction: 'desc'
            }
        },
        {
            name: 'Más reciente',
            value: {
                property: 'lastUpdate',
                direction: 'asc'
            }
        }
    ];

    ScopeService.getScope($routeParams.idScope)
        .success(function (data, status) {
            if (status == 200) {
                $scope.scope = data;
            }
        });

    $scope.getScopeVersions = function () {
        $scope.loading = true;
        HistoryService.getScopeHistory($routeParams.idScope, $scope.scopePage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.versions = data.content;
                    $scope.totalVersions = parseInt(data.totalElements) + 1;
                    $scope.scopePage.totalPages = data.totalPages;
                    $scope.loading = false;
                }
            });
    }

    $scope.getScopeVersions();

    $scope.$watch('scopePage.sort.property', function () {
        if ($scope.terms) {
            $scope.getScopeVersions();
        }
    });

    $scope.$watch('scopePage.sort.direction', function () {
        if ($scope.versions) {
            $scope.getScopeVersions();
        }
    });

    $scope.$watch('scopePage.number', function () {
        if ($scope.versions) {
            $scope.getScopeVersions();
        }
    });

    $scope.$watch('scopePage.size', function () {
        if ($scope.versions) {
            $scope.getScopeVersions();
        }
    });

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.previousPage = function () {
        $scope.scopePage.number--;
    };

    $scope.nextPage = function () {
        $scope.scopePage.number++;
    };

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_SCOPE_UPDATE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }
});
