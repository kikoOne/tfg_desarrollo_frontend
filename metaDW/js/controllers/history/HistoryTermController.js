/**
 * Created by jguzman on 20/07/2016.
 */

angular.module('metaDW').controller('HistoryTermController', function ($rootScope, $scope, $routeParams, HistoryService, TermService) {

    $rootScope.contentHead = 'Historial de término';

    $scope.termPage = {
        number: 0,
        size: 20,
        sort: {
            property: 'lastUpdate',
            direction: 'desc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            name: 'Más antiguo',
            value: {
                property: 'lastUpdate',
                direction: 'desc'
            }
        },
        {
            name: 'Más reciente',
            value: {
                property: 'lastUpdate',
                direction: 'asc'
            }
        }
    ];

    var idTerm = $routeParams.idTerm;
    TermService.getTerm(idTerm)
        .success(function (data, status) {
            if (status == 200) {
                $scope.term = data;
            }
        });

    $scope.getTermVersions = function () {
        $scope.loading = true;
        HistoryService.getTermHistory(idTerm, $scope.termPage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.versions = data.content;
                    $scope.totalVersions = parseInt(data.totalElements) + 1;
                    $scope.termPage.totalPages = data.totalPages;
                    $scope.loading = false;
                }
            });
    };
    $scope.getTermVersions();

    $scope.$watch('termPage.sort.property', function () {
        if ($scope.terms) {
            $scope.getTermVersions();
        }
    });

    $scope.$watch('termPage.sort.direction', function () {
        if ($scope.versions) {
            $scope.getTermVersions();
        }
    });

    $scope.$watch('termPage.number', function () {
        if ($scope.versions) {
            $scope.getTermVersions();
        }
    });

    $scope.$watch('termPage.size', function () {
        if ($scope.versions) {
            $scope.getTermVersions();
        }
    });

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.previousPage = function () {
        $scope.termPage.number--;
    };

    $scope.nextPage = function () {
        $scope.termPage.number++;
    };

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_TERM_UPDATE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }
});
