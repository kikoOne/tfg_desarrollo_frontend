/**
 * Created by jguzman on 18/07/2016.
 */

angular.module('metaDW').controller('HistoryCategoryController', function ($rootScope, $scope, $routeParams, HistoryService, CategoryService) {

    $rootScope.contentHead = 'Historial de categoría';
    
    $scope.categoryPage = {
        number: 0,
        size: 20,
        sort: {
            property: 'lastUpdate',
            direction: 'desc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            name: 'Más antiguo',
            value: {
                property: 'lastUpdate',
                direction: 'desc'
            }
        },
        {
            name: 'Más reciente',
            value: {
                property: 'lastUpdate',
                direction: 'asc'
            }
        }
    ];

    CategoryService.getCategory($routeParams.idCategory)
        .success(function (data, status) {
            if (status == 200) {
                $scope.category = data;
            }
        });

    $scope.getCategoryVersions = function () {
        $scope.loading = true;
        HistoryService.getCategoryHistory($routeParams.idCategory, $scope.categoryPage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.versions = data.content;
                    $scope.totalVersions = parseInt(data.totalElements) + 1;
                    $scope.categoryPage.totalPages = data.totalPages;
                    $scope.loading = false;
                }
            });
    }

    $scope.getCategoryVersions();

    $scope.$watch('categoryPage.sort.property', function () {
        if ($scope.versions) {
            $scope.getCategoryVersions();
        }
    });

    $scope.$watch('categoryPage.sort.direction', function () {
        if ($scope.versions) {
            $scope.getCategoryVersions();
        }
    });

    $scope.$watch('categoryPage.number', function () {
        if ($scope.versions) {
            $scope.getCategoryVersions();
        }
    });

    $scope.$watch('categoryPage.size', function () {
        if ($scope.versions) {
            $scope.getCategoryVersions();
        }
    });

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.previousPage = function () {
        $scope.categoryPage.number--;
    };

    $scope.nextPage = function () {
        $scope.categoryPage.number++;
    };

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_CATEGORY_UPDATE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }
});
