/**
 * Created by jguzman on 08/06/2016.
 */

angular.module('metaDW').controller('CategoryController', function ($scope, $rootScope, $routeParams, CategoryService, TermService, SearchService, $location) {

        $scope.searchParam = '';
        $scope.sort = [];
        $scope.listPage = { //paginado y orden por defecto
            number: 0,
            size: 20,
            sort: {
                property: '',
                direction: 'asc'
            },
            totalPages: ''
        }

        $scope.subcategoriesSortList = [
            {
                name: 'Nombre',
                value: 'child.name'
            },
            {
                name: 'Fecha de creación',
                value: 'child.creation'
            },
            {
                name: 'Autor',
                value: 'child.owner.name'
            }
        ];

        $scope.termSortList = [
            {
                name: 'Nombre',
                value: 'term.name'
            },
            {
                name: 'Fecha de creación',
                value: 'term.creation'
            },
            {
                name: 'Autor',
                value: 'term.owner.name'
            }
        ];

        /**
         * Acciones
         */
        $scope.isEditable = function () {
            return hasRole('ROLE_CATEGORY_UPDATE')
        };

        $scope.isErasable = function () {
            return hasRole('ROLE_CATEGORY_DELETE')
        };

        function hasRole(role) {
            var flag = false;
            if ($rootScope.myMetaDWUser) {
                $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                    if (role == entry.authority) {
                        flag = true;
                    }
                });
            }
            return flag;
        }

        $scope.selected = [];
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.listDenomination = function (chip) {
            return {
                name: chip.denomination,
                type: 'unknown'
            };
        };

        $scope.getTerms = function () {
            $scope.loading = true;
            CategoryService.getTermsByCategory($routeParams.idCategory, $scope.listPage.number, $scope.listPage.size,
                    $scope.listPage.sort.property + ',' + $scope.listPage.sort.direction)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.terms = data.content;
                        $scope.listPage.totalPages = data.totalPages;
                        $scope.totalElements = data.totalElements;
                        $scope.terms.forEach(function (entry) {
                            TermService.getDenominationsFromTerm(entry.idTerm, $scope.listPage.number, $scope.listPage.size, '')
                                .success(function (data, status) {
                                    if (status == 200) {
                                        entry.denominations = data.content;
                                    }
                                });
                        });
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $scope.loading = false;
                });
        };

        $scope.getCategories = function () {
            $scope.loading = true;
            CategoryService.getCategoriesFromCategory($routeParams.idCategory, $scope.listPage.number, $scope.listPage.size,
                    $scope.listPage.sort.property + ',' + $scope.listPage.sort.direction)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.categories = data.content;
                        $scope.totalElements = data.totalElements;
                        $scope.listPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $scope.loading = false;
                });
        };

        $scope.getTermTypes = function () {
            TermService.getTermTypes()
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.termTypes = data;
                        $scope.termTypes.forEach(function (entry) {
                            $scope.toggle(entry, $scope.selected);
                        });
                    }
                });
        };

        CategoryService.getCategory($routeParams.idCategory)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.category = data;
                    $rootScope.contentHead = data.name;
                    if ($scope.category.type != 2) {
                        $scope.sort = $scope.subcategoriesSortList;
                        $scope.listPage.sort.property = $scope.sort[0].value;
                        $scope.getCategories();
                    } else if ($scope.category.type == 2) {
                        $scope.sort = $scope.termSortList;
                        $scope.listPage.sort.property = $scope.sort[0].value;
                        $scope.getTerms();
                        $scope.getTermTypes();
                    }
                }
            });

        $scope.removeCategory = function () {
            CategoryService.deleteCategory($routeParams.idCategory)
                .success(function (data, status) {
                    if (status == 204) {
                        $location.path('/categories');
                    }
                });
        };

        $scope.download = function () {
            var creation = new Date($scope.category.creation);
            var day = creation.getDay();
            if (day < 10) {
                day = '0' + day;
            }
            var month = creation.getMonth();
            if (month < 10) {
                month = '0' + month;
            }
            var year = creation.getFullYear();
            var _creation = day + ' - ' + month + ' - ' + year;

            var lastUpdate = new Date($scope.category.lastUpdate);
            var day = lastUpdate.getDay();
            if (day < 10) {
                day = '0' + day;
            }
            var month = lastUpdate.getMonth();
            if (month < 10) {
                month = '0' + month;
            }
            var year = lastUpdate.getFullYear();
            var _lastUpdate = day + ' - ' + month + ' - ' + year;

            if ($scope.category.definition)
                var definition = $scope.category.definition;
            else
                var definition = '-';

            var docDefinition = {
                content: [
                    {text: $scope.category.name, style: 'header'},
                    {text: ' ', style: 'header'},
                    {
                        table: {
                            // headers are automatically repeated if the table spans over multiple pages
                            // you can declare how many rows should be treated as headers
                            headerRows: 1,
                            widths: ['auto', '*'], //'auto', 100,
                            body: [
                                ['Tipo', 'Categoría'],
                                ['Autor', $scope.category.owner.name + ' ' + $scope.category.owner.surnames],
                                ['Fecha de creación', _creation],
                                ['Fecha de actualización', _lastUpdate],
                                ['Versión', $scope.category.version],
                                ['Descripción', definition]
                            ]
                        }
                    }
                ],
                styles: {
                    header: {
                        fontSize: 22,
                        bold: true,
                        alignment: 'center'
                    },
                    cursive: {
                        bolditalic: true,
                        alignment: 'left'
                    }
                }
            };
            pdfMake.createPdf(docDefinition).open();
        }

        $scope.mouseenter = function (item) {
            item.hover = true;
        };

        $scope.mouseleave = function (item) {
            item.hover = false;
        };

        $scope.$watch('listPage.sort.property', function () {
            if ($scope.categories) {
                $scope.searchSubcategories();
            } else if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.$watch('listPage.number', function () {
            if ($scope.categories) {
                $scope.searchSubcategories();
            } else if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.$watch('listPage.size', function () {
            if ($scope.categories) {
                $scope.searchSubcategories();
            } else if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.searchTerms = function () {
            var selectedTypes = [];
            $scope.selected.forEach(function (entry) {
                selectedTypes.push(entry.type);
            });
            $scope.loading = true;
            SearchService.findTermsByNameAndTypeFromCategory($scope.listPage, $scope.searchParam, selectedTypes, $routeParams.idCategory)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.terms = data.content;
                        $scope.listPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $scope.loading = false;
                });
        };

        $scope.searchSubcategories = function () {
            $scope.loading = true;
            SearchService.findCategoriesByNameFromCategory($scope.listPage, $scope.searchParam, $routeParams.idCategory)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.categories = data.content;
                        $scope.listPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $scope.loading = false;
                });
        };

        var flagFirst = true;
        $scope.$watchCollection('selected', function () {
            // if ($scope.selected.length > 0 && !searching && !flagFirst) {
            //     searching = true;
            $scope.searchTerms();
            // } else if ($scope.selected.length > 0 && !searching && flagFirst) {
            //     flagFirst = false;
            // }
        });

        $scope.$watch('searchParam', function () {
            if ($scope.categories) {
                $scope.searchSubcategories();
            } else if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.previousPage = function () {
            $scope.categoryListPage.number--;
        };

        $scope.nextPage = function () {
            $scope.categoryListPage.number++;
        };
    }
);