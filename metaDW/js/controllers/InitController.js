/**
 * Created by jguzman on 01/08/2016.
 */

angular.module('metaDW').controller('InitController', function ($rootScope, $scope, TermService, IncidenceService, JWTHelper, UserService, LocalStorageService, $timeout) {

    $rootScope.contentHead = '';

    $scope.termPage = {
        number: 0,
        size: 10,
        sort: {
            direction: 'desc',
            property: 'lastUpdate'
        },
        totalPages: 1
    };

    /**
     * Recuperar novedades
     */
    $scope.getNews = function () {
        $scope.loadingTerms = true;
        TermService.getTerms($scope.termPage)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.terms = [];
                    data.content.forEach(function (entry) {
                        $scope.terms.push(entry);
                    });
                    $scope.termPage.totalPages = data.totalPages;
                    $scope.termPage.number = data.number;
                    $scope.loadingTerms = false;
                } else {
                    $scope.loadingTerms = false;
                }
            });
    }
    $scope.getNews();

    /**
     * Recuperar últimas incidencias
     */
    $scope.getIncidences = function () {
        $scope.loadingIncidences = true;
        IncidenceService.getLastIncidences()
            .success(function (data, status) {
                if (status == 200) {
                    $timeout(function () {
                        $scope.incidences = data.content;
                    }, 650);
                    $scope.loadingIncidences = false;
                } else {
                    $scope.loadingIncidences = false;
                }
            });
    }
    $scope.getIncidences();

    /**
     * Recuperar mis marcadores
     */
    $scope.getBookmarks = function () {
        if (LocalStorageService.getObject('myMetaDWUser')) {
            $scope.loadingBookmarks = true;
            UserService.getAllTermBookmarks(JWTHelper.getTokenId())
                .success(function (data, status) {
                    if (status == 200) {
                        $timeout(function () {
                            $scope.bookmarks = [];
                            data.forEach(function (entry) {
                                $scope.bookmarks.push(entry.term);
                            });
                        }, 650);
                        $scope.loadingBookmarks = false;
                    } else {
                        $scope.loadingBookmarks = false;
                    }
                });
        }
    }
    $scope.getBookmarks();

    //Carga de página
    var alreadyloading = false;
    var offset = 50;

    $(window).scroll(function () {
        if ($(document).height() == ($(window).height() + $(window).scrollTop())) {

        }
    });
});

