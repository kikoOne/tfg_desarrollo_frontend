/**
 * Created by jguzman on 24/05/2016.
 */

angular.module('metaDW').controller('UserListController', function ($scope, $rootScope, UserService, AuthenticationService, $mdDialog, SearchService, $mdToast, PendingRequestService) {

    $rootScope.contentHead = 'Administración de usuarios';
    $scope.selectedTab = 0;

    $scope.userListPage = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'userName',
            direction: 'asc'
        },
        totalPages: 1
    }
    $scope.desactivatedUserPage = 0;
    $scope.totalDesactivatedPages = 1;

    $scope.sortList = [
        {
            name: 'Nombre de usuario',
            value: 'userName'
        },
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Apellidos',
            value: 'surnames'
        },
        {
            name: 'Fecha de registro',
            value: 'register'
        },
        {
            name: 'Último acceso',
            value: 'lastLogin'
        }
    ];
    var flag = false;
    $scope.getUsers = function () {
        $scope.loading = true;
        UserService.getUsers($scope.desactivatedUserPage, $scope.userListPage.size, $scope.userListPage.sort.property + ',' + $scope.userListPage.sort.direction)
            .success(function (data, status, header) {
                if (status == 200) {
                    console.log(header['url']);
                    $scope.users = data.content;
                    $scope.userListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                    flag = true;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.getUsers();

    $scope.removeUser = function (user) {
        UserService.deleteUser(user.idUser)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.getUsers();
                    $mdToast.show($mdToast.simple().textContent('Usuario desactivado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al desactivar usuario').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al desactivar usuario').position('bottom left').hideDelay(3000));
            });
    };

    $scope.getDesactivatedUsers = function () {
        $scope.loading = true;
        UserService.getDesactivatedUsers($scope.userListPage.number, $scope.userListPage.size, $scope.userListPage.sort.property + ',' + $scope.userListPage.sort.direction)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.desactivatedUsers = data.content;
                    $scope.totalDesactivatedPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.activateUser = function (user) {
        UserService.activateUser(user.idUser)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.selectedTab = 0;
                    $scope.getUsers();
                    $mdToast.show($mdToast.simple().textContent('Usuario activado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al activar usuario').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al activar usuario').position('bottom left').hideDelay(3000));
            });
    };

    $scope.$watch('selectedTab', function () {
        if ($scope.selectedTab == 0) {
            $scope.users = null;
            $scope.getUsers();
        } else {
            $scope.desactivatedUsers = null;
            $scope.getDesactivatedUsers();
        }
    });

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.$watch('userListPage.sort.property', function () {
        if ($scope.users && $scope.selectedTab == 0 && flag)
            $scope.getUsers();
        else if ($scope.users && $scope.selectedTab == 1)
            $scope.getDesactivatedUsers();
    });

    $scope.$watch('userListPage.number', function () {
        if (flag)
            $scope.getUsers();
    });

    $scope.$watch('desactivatedUserPage', function () {
        $scope.getDesactivatedUsers();
    });

    $scope.$watch('userListPage.size', function () {
        if ($scope.users && $scope.selectedTab == 0 && flag)
            $scope.getUsers();
        else if ($scope.users && $scope.selectedTab == 1)
            $scope.getDesactivatedUsers();
    });

    $scope.$watch('searchParam', function () {
        if ($scope.loading == true) {
            PendingRequestService.cancelAll();
        }
        // else {
        $scope.loading = true;
        if ($scope.users && $scope.selectedTab == 0 && flag) {
            SearchService.findUsersByName($scope.userListPage, $scope.searchParam)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.users = data.content;
                        $scope.userListPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    }
                });
        } else if ($scope.users && $scope.selectedTab == 1 && flag) {
            SearchService.findDesactivatedUsersByName($scope.userListPage, $scope.searchParam)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.desactivatedUsers = data.content;
                        $scope.userListPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    }
                });
        }
        // }
    });

    $scope.previousPage = function () {
        $scope.userListPage.number--;
    };

    $scope.nextPage = function () {
        $scope.userListPage.number++;
    };

    $scope.previousDesactivatedPage = function () {
        $scope.desactivatedUserPage--;
    };

    $scope.nextDesactivatedPage = function () {
        $scope.desactivatedUserPage++;
    };

    /**
     * Abrir popup de creación de usuario
     * @param ev
     */
    $scope.showNewUserDialog = function (ev) {
        $mdDialog.show({
                controller: NewUserController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newUserDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {
                if (send == 201) {
                    $scope.getUsers();
                    $mdToast.show($mdToast.simple().textContent('Usuario añadido').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al añadir usuario').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de creación de usuario
     * @param $scope
     * @param $mdDialog
     * @param UserService
     * @constructor
     */
    function NewUserController($scope, $mdDialog, UserService) {
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            $scope.newUser.password = $scope.newUser.userName;
            $scope.newUser.state = 1;
            $scope.newUser.roles = $scope.selectedRoles;

            UserService.createUser($scope.newUser)
                .success(function (data, status) {
                    if (status == 201) { //HTTPSTATUS.CREATED
                        $mdDialog.hide(201);
                    }
                });
        };

        $scope.selectedRoles = [];
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };
        $scope.getAllRoles = function () {
            AuthenticationService.getAllRoles()
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.roles = data;
                        $scope.roles.forEach(function (entry) {
                            $scope.toggle(entry, $scope.selectedRoles);
                        });
                    }
                });
        }
        $scope.getAllRoles();
    }

    /**
     * Abrir popup de edición de usuario
     * @param ev
     */
    $scope.showEditUserDialog = function (ev, user) {
        $mdDialog.show({
                controller: EditUserController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newUserDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    item: user
                }
            })
            .then(function (send) {
                if (send == 200) {
                    $scope.getUsers();
                    $mdToast.show($mdToast.simple().textContent('Usuario editado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al editar usuario').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de edición de usuario
     * @param $scope
     * @param $mdDialog
     * @param UserService
     * @constructor
     */
    function EditUserController($scope, $mdDialog, UserService, item) {

        var user = {
            idUser: item.idUser,
            email: item.email,
            userName: item.userName,
            name: item.name,
            surnames: item.surnames,
            register: new Date(item.register),
            roles: []
        }

        item.roles.forEach(function (entry) {
            user.roles.push(entry);
        });

        $scope.newUser = user;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            $scope.newUser.roles = $scope.selectedRoles;
            UserService.updateUser($scope.newUser.idUser, $scope.newUser)
                .success(function (data, status) {
                    if (status == 200) { //HTTPSTATUS.CREATED
                        $mdDialog.hide(200);
                    }
                });
        };

        $scope.selectedRoles = [];
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };
        $scope.getAllRoles = function () {
            AuthenticationService.getAllRoles()
                .success(function (data, status) {
                    if (status == 200) {

                        $scope.roles = data;
                        $scope.roles.forEach(function (role) {
                            if (contains($scope.newUser.roles, role)) { //si el rol está contenido en los roles del usuario
                                $scope.toggle(role, $scope.selectedRoles);
                            }
                        });
                    }
                });
        }
        $scope.getAllRoles();

        function contains(list, item) {
            var flag = false;
            list.forEach(function (entry) {
                if (item.idRole == entry.idRole) {
                    flag = true;
                } else {

                }
            });
            return flag;
        }
    }
});
