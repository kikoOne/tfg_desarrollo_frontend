/**
 * Created by jguzman on 20/07/2016.
 */

angular.module('metaDW').controller('IncidenceListController', function ($rootScope, $scope, IncidenceService, JWTHelper, $mdDialog, $mdToast) {

    $rootScope.contentHead = 'Administración de incidencias';

    $scope.page = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'idTerm.name',
            direction: 'asc'
        },
        totalPages: 1
    };

    $scope.sortList = [
        {
            name: 'Término',
            value: 'idTerm.name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        }
    ];

    $scope.getIncidences = function () {
        $scope.loading = true;
        IncidenceService.getFullIncidences($scope.page)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.incidences = data.content;
                    $scope.page.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.getIncidences();

    $scope.removeIncidence = function (incidence) {
        IncidenceService.removeIncidence(incidence.idIncidence, incidence.idTerm.idTerm)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.getIncidences();
                    $mdToast.show($mdToast.simple().textContent('Incidencia eliminada').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar incidencia').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar incidencia').position('bottom left').hideDelay(3000));
            });
    };

    //PAGES
    $scope.previousPage = function () {
        $scope.page.number--;
    };

    $scope.nextPage = function () {
        $scope.page.number++;
    };

    //WATCH
    $scope.$watch('page.sort.property', function () {
        if ($scope.incidences)
            $scope.getIncidences();
    });

    $scope.$watch('page.number', function () {
        if ($scope.incidences)
            $scope.getIncidences();
    });

    $scope.$watch('page.size', function () {
        if ($scope.incidences)
            $scope.getIncidences();
    });

    //MOUSEOVER
    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    /**
     * Abrir popup de creación de incidencia
     * @param ev
     */
    $scope.showNewIncidenceDialog = function (ev) {
        $mdDialog.show({
                controller: NewIncidenceController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newIncidenceDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {
                if (send == 201) {
                    $scope.getIncidences();
                    $mdToast.show($mdToast.simple().textContent('Incidencia creada').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al crear incidencia').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Abrir popup de edición de incidencia
     * @param ev
     */
    $scope.showEditIncidenceDialog = function (ev, item) {

        $mdDialog.show({
                controller: EditIncidenceController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newIncidenceDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    item: item
                }
            })
            .then(function (send) {
                if (send == 200) {
                    $scope.getIncidences();
                    $mdToast.show($mdToast.simple().textContent('Incidencia editada').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al editar incidencia').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de nueva incidencia
     * @param $scope
     * @param $mdDialog
     * @param IncidenceService
     * @param SearchService
     * @constructor
     */
    function NewIncidenceController($scope, $mdDialog, IncidenceService, SearchService) {

        $scope.termQuery = '';
        $scope.newIncidence = {
            creation: new Date()
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {

            IncidenceService.createIncidence($scope.newIncidence)
                .success(function (data, status) {
                    if (status == 201) { //HTTPSTATUS.CREATED
                        $mdDialog.hide(201);
                    }
                });
        };

        $scope.termListPage = { //paginado y orden por defecto
            number: 0,
            size: 20,
            sort: {
                property: 'name',
                direction: 'asc'
            },
            totalPages: 1
        };

        $scope.findTerms = function () {
            SearchService.findTermsByName($scope.termListPage, $scope.termQuery)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.terms = data.content;
                    }
                });
        };
        $scope.findTerms();

        $scope.selectedTerm = function (input) {
            if (input)
                $scope.newIncidence.idTerm = input.idTerm;
            else
                $scope.newIncidence.idTerm = null;
        };
    }

    /**
     * Controlador de edición de incidencia
     * @param $scope
     * @param $mdDialog
     * @param IncidenceService
     * @param SearchService
     * @constructor
     */
    function EditIncidenceController($scope, $mdDialog, IncidenceService, SearchService, item) {

        $scope.termQuery = '';
        var incidence = {
            idIncidence: item.idIncidence,
            idTerm: item.idTerm,
            creation: new Date(item.creation),
            description: item.description
        };
        $scope.newIncidence = incidence;
        //$scope.newIncidence.creation = new Date($scope.newIncidence.creation);
        $scope.selectedTermItem = incidence.idTerm;
        //$scope.newIncidence.idTerm = item.idTerm.idTerm;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            var incidence = $scope.newIncidence;
            incidence.idTerm = $scope.selectedTermItem.idTerm;
            // $scope.newIncidence.idTerm = $scope.selectedTermItem.idTerm;
            IncidenceService.updateIncidence($scope.newIncidence.idIncidence, incidence)
                .success(function (data, status) {
                    if (status == 200) { //HTTPSTATUS.OK
                        $mdDialog.hide(200);
                    }
                });
        };

        $scope.termListPage = { //paginado y orden por defecto
            number: 0,
            size: 20,
            sort: {
                property: 'name',
                direction: 'asc'
            },
            totalPages: 1
        };

        $scope.findTerms = function () {
            SearchService.findTermsByName($scope.termListPage, $scope.termQuery)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.terms = data.content;
                    }
                });
        };
        $scope.findTerms();

        $scope.selectedTerm = function (input) {
            if (input)
                $scope.newIncidence.idTerm = input.idTerm;
            else
                $scope.newIncidence.idTerm = null;
        };
    }
});
