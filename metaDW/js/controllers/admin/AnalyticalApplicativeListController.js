/**
 * Created by jguzman on 04/07/2016.
 */

angular.module('metaDW').controller('AnalyticalApplicativeListController', function ($rootScope, $scope, JWTHelper, AnalyticalApplicativeService, SearchService, $mdDialog, $mdToast) {

    $rootScope.contentHead = 'Administración de aplicativos analíticos';

    $scope.searchParam = '';
    $scope.page = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: 1
    };

    $scope.getAnalyticalApplicatives = function () {
        $scope.loading = true;
        SearchService.findAnalyticalApplicativesByName($scope.page, $scope.searchParam)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.analyticalApplicatives = data.content;
                    $scope.page.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    }

    $scope.getAnalyticalApplicatives();

    $scope.$watch('page.number', function () {
        if ($scope.analyticalApplicatives)
            $scope.getAnalyticalApplicatives();
    });

    $scope.$watch('page.size', function () {
        if ($scope.analyticalApplicatives)
            $scope.getAnalyticalApplicatives();
    });

    $scope.$watch('searchParam', function () {
        if ($scope.analyticalApplicatives)
            $scope.getAnalyticalApplicatives();
    });

    $scope.previousPage = function () {
        $scope.page.number--;
    };

    $scope.nextPage = function () {
        $scope.page.number++;
    };

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.removeApplicative = function (applicative) {
        AnalyticalApplicativeService.removeAnalyticalApplicatives(applicative.idApplicative)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.getAnalyticalApplicatives();
                    $mdToast.show($mdToast.simple().textContent('Aplicativo eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar aplicativo').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar aplicativo').position('bottom left').hideDelay(3000));
            });
    };

    /**
     * Abrir popup de edición de usuario
     * @param ev
     */
    $scope.showEditAnalyticalApplicativeDialog = function (ev, applicative) {
        $mdDialog.show({
                controller: EditAnalyticalApplicativeController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newAnalyticalApplicativeDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    item: applicative
                }
            })
            .then(function (send) {
                if (send == 200) {
                    $scope.getAnalyticalApplicatives();
                    $mdToast.show($mdToast.simple().textContent('Aplicativo editado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al editar aplicativo').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de nuevo aplicativo analítico
     *
     * @param $scope
     * @param $mdDialog
     * @constructor
     */
    function EditAnalyticalApplicativeController($scope, $mdDialog, item) {

        var applicative = {
            idApplicative: item.idApplicative,
            name: item.name,
            description: item.description
        };
        $scope.analyticalApplicative = applicative;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            AnalyticalApplicativeService.updateAnalyticalApplicative($scope.analyticalApplicative.idApplicative, $scope.analyticalApplicative)
                .success(function (data, status) {
                    if (status == 200) { //HTTPSTATUS.OK
                        $mdDialog.hide(200);
                    }
                });
        };
    }

    /**
     * Abrir popup de creación de usuario
     * @param ev
     */
    $scope.showNewAnalyticalApplicativeDialog = function (ev) {
        $mdDialog.show({
                controller: NewAnalyticalApplicativeController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newAnalyticalApplicativeDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {
                if (send == 201) {
                    $scope.getAnalyticalApplicatives();
                    $mdToast.show($mdToast.simple().textContent('Aplicativo creado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al crear aplicativo').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de nuevo aplicativo analítico
     *
     * @param $scope
     * @param $mdDialog
     * @constructor
     */
    function NewAnalyticalApplicativeController($scope, $mdDialog) {
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            AnalyticalApplicativeService.createAnalyticalApplicative($scope.analyticalApplicative)
                .success(function (data, status) {
                    if (status == 201) { //HTTPSTATUS.CREATED
                        $mdDialog.hide(201);
                    }
                });
        };
    }
});
