/**
 * Created by jguzman on 05/07/2016.
 */

angular.module('metaDW').controller('OperationalOriginListController', function ($rootScope, $scope, $mdDialog, OperationalOriginService, SearchService, $mdToast) {

    $rootScope.contentHead = 'Administración de orígenes operacionales';

    $scope.searchParam = '';
    $scope.page = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: 1
    };

    $scope.getOperationalOrigins = function () {
        $scope.loading = true;
        SearchService.findOperationalOriginsByName($scope.page, $scope.searchParam)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.loading = false;
                    $scope.operationalOrigins = data.content;
                    $scope.page.totalPages = data.totalPages;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.getOperationalOrigins();

    $scope.$watch('page.number', function () {
        if ($scope.getOperationalOrigins)
            $scope.getOperationalOrigins();
    });

    $scope.$watch('page.size', function () {
        if ($scope.getOperationalOrigins)
            $scope.getOperationalOrigins();
    });

    $scope.$watch('searchParam', function () {
        if ($scope.getOperationalOrigins)
            $scope.getOperationalOrigins();
    });

    $scope.previousPage = function () {
        $scope.page.number--;
    };

    $scope.nextPage = function () {
        $scope.page.number++;
    };

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.removeOrigin = function (operationalOrigin) {
        OperationalOriginService.removeOperationalOrigin(operationalOrigin.idOrigin)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.getOperationalOrigins();
                    $mdToast.show($mdToast.simple().textContent('Origen eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al editar usuario').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al editar usuario').position('bottom left').hideDelay(3000));
            });
    };

    /**
     * Abrir popup de edición de origen operacional
     * @param ev
     */
    $scope.showEditOperationalOriginDialog = function (ev, origin) {
        $mdDialog.show({
                controller: EditOperationalOriginController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newOperationalOriginDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    item: origin
                }
            })
            .then(function (send) {
                if (send == 200) {
                    $scope.getOperationalOrigins();
                    $mdToast.show($mdToast.simple().textContent('Origen editado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al editar origen').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de edición de origen operacional
     *
     * @param $scope
     * @param $mdDialog
     * @constructor
     */
    function EditOperationalOriginController($scope, $mdDialog, item) {

        var origin = {
            idOrigin: item.idOrigin,
            name: item.name,
            description: item.description
        };
        $scope.operationalOrigin = origin;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            OperationalOriginService.updateOperationalOrigin($scope.operationalOrigin.idOrigin, $scope.operationalOrigin)
                .success(function (data, status) {
                    if (status == 200) { //HTTPSTATUS.OK
                        $mdDialog.hide(200);
                    }
                });
        };
    }

    /**
     * Abrir popup de creación de origen operacional
     * @param ev
     */
    $scope.showNewOperationalOriginDialog = function (ev) {
        $mdDialog.show({
                controller: NewOperationalOriginController,
                templateUrl: '/tfg_desarrollo_frontend/metaDW/templates/admin/partials/newOperationalOriginDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (send) {
                if (send == 201) {
                    $scope.getOperationalOrigins();
                    $mdToast.show($mdToast.simple().textContent('Origen creado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al crear origen').position('bottom left').hideDelay(3000));
                }
            });
    };

    /**
     * Controlador de nuevo origen operacional
     *
     * @param $scope
     * @param $mdDialog
     * @constructor
     */
    function NewOperationalOriginController($scope, $mdDialog) {
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send = function () {
            OperationalOriginService.createOperationalOrigin($scope.operationalOrigin)
                .success(function (data, status) {
                    if (status == 201) { //HTTPSTATUS.CREATED
                        $mdDialog.hide(201);
                    }
                });
        };
    }
});
