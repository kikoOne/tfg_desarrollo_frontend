/**
 * Created by jguzman on 08/06/2016.
 */

angular.module('metaDW').controller('ScopeController', function ($scope, $rootScope, ScopeService, TermService, $routeParams, SearchService, $location) {

        $scope.searchParam = '';
        $scope.termListPage = { //paginado y orden por defecto
            number: 0,
            size: 20,
            sort: {
                property: 'term.name',
                direction: 'asc'
            },
            totalPages: ''
        }

        $scope.sortList = [
            {
                name: 'Nombre',
                value: 'term.name'
            },
            {
                name: 'Fecha de creación',
                value: 'term.creation'
            },
            {
                name: 'Autor',
                value: 'term.owner.name'
            }
        ];

        /**
         * Acciones
         */
        $scope.isEditable = function () {
            return hasRole('ROLE_SCOPE_UPDATE')
        };

        $scope.isErasable = function () {
            return hasRole('ROLE_SCOPE_DELETE')
        };

        function hasRole(role) {
            var flag = false;
            if ($rootScope.myMetaDWUser) {
                $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                    if (role == entry.authority) {
                        flag = true;
                    }
                });
            }
            return flag;
        }

        $scope.selected = [];
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };
        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        TermService.getTermTypes()
            .success(function (data, status) {
                if (status == 200) {
                    $scope.termTypes = data;
                    $scope.termTypes.forEach(function (entry) {
                        $scope.toggle(entry, $scope.selected);
                    });
                }
            });

        ScopeService.getScope($routeParams.idScope)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scope = data;
                    $rootScope.contentHead = data.name;
                }
            });

        $scope.loading = true;
        $scope.getTerms = function () {
            ScopeService.getTermsByScope($routeParams.idScope, $scope.termListPage.number, $scope.termListPage.size,
                    $scope.termListPage.sort.property + ',' + $scope.termListPage.sort.direction)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.terms = data.content;
                        $scope.termListPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $scope.loading = false;
                });
        };
        $scope.getTerms();

        $scope.searchTerms = function () {
            var selectedTypes = [];
            $scope.selected.forEach(function (entry) {
                selectedTypes.push(entry.type);
            });
            $scope.loading = true;
            SearchService.findTermsByNameAndTypeFromScope($scope.termListPage, $scope.searchParam, selectedTypes, $routeParams.idScope)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.terms = data.content;
                        $scope.termListPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function (response) {
                    $scope.loading = false;
                });
        };

        $scope.removeScope = function (scope) {
            ScopeService.deleteScope(scope.idScope)
                .success(function (data, status) {
                    if (status == 204) {
                        $location.path('/scopes');
                    }
                });
        };

        $scope.download = function () {
            var creation = new Date($scope.scope.creation);
            var day = creation.getDay();
            if (day < 10) {
                day = '0' + day;
            }
            var month = creation.getMonth();
            if (month < 10) {
                month = '0' + month;
            }
            var year = creation.getFullYear();
            var _creation = day + ' - ' + month + ' - ' + year;

            var lastUpdate = new Date($scope.scope.lastUpdate);
            var day = lastUpdate.getDay();
            if (day < 10) {
                day = '0' + day;
            }
            var month = lastUpdate.getMonth();
            if (month < 10) {
                month = '0' + month;
            }
            var year = lastUpdate.getFullYear();
            var _lastUpdate = day + ' - ' + month + ' - ' + year;

            if ($scope.scope.definition)
                var definition = $scope.scope.definition;
            else
                var definition = '-';

            var docDefinition = {
                content: [
                    {text: $scope.scope.name, style: 'header'},
                    {text: ' ', style: 'header'},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['auto', '*'], //'auto', 100,
                            body: [
                                ['Tipo', 'Ámbito'],
                                ['Autor', $scope.scope.owner.name + ' ' + $scope.scope.owner.surnames],
                                ['Fecha de creación', _creation],
                                ['Fecha de actualización', _lastUpdate],
                                ['Versión', $scope.scope.version],
                                ['Descripción', definition]
                            ]
                        }
                    }
                ],
                styles: {
                    header: {
                        fontSize: 22,
                        bold: true,
                        alignment: 'center'
                    },
                    cursive: {
                        bolditalic: true,
                        alignment: 'left'
                    }
                }
            };
            pdfMake.createPdf(docDefinition).open();
        };

        $scope.$watch('termListPage.sort.property', function () {
            if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.$watch('termListPage.number', function () {
            if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.$watch('termListPage.size', function () {
            if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.$watchCollection('selected', function () {
            if ($scope.selected.length > 0) {
                $scope.searchTerms();
            }
        });

        $scope.$watch('searchParam', function () {
            if ($scope.terms) {
                $scope.searchTerms();
            }
        });

        $scope.previousPage = function () {
            $scope.termListPage.number--;
        };

        $scope.nextPage = function () {
            $scope.termListPage.number++;
        };
    }
);
