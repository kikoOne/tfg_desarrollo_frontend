/**
 * Created by jguzman on 10/06/2016.
 */

angular.module('metaDW').controller('TermController', function ($scope, $rootScope, $routeParams, TermService, UserService, JWTHelper, $location, $mdToast) {

    var idTerm = $routeParams.idTerm;
    var idUser = JWTHelper.getTokenId();
    $scope.page = { //paginado para recuperar
        number: '0',
        size: '100'
    }

    /**
     * Acciones
     */
    $scope.isEditable = function () {
        return hasRole('ROLE_TERM_UPDATE')
    };

    $scope.isErasable = function () {
        return hasRole('ROLE_TERM_DELETE')
    };

    function hasRole(role) {
        var flag = false;
        if ($rootScope.myMetaDWUser) {
            $rootScope.myMetaDWUser.roles.forEach(function (entry) {
                if (role == entry.authority) {
                    flag = true;
                }
            });
        }
        return flag;
    }

    /**
     * Marcadores
     * @type {boolean}
     */
    $scope.hasBookmark = false;
    if (idUser) {
        UserService.getTermBookmark(idUser, idTerm)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.hasBookmark = true;
                }
            });
    }

    $scope.addBookmark = function () {
        UserService.addTermBookmark(idUser, idTerm)
            .success(function (data, status) {
                if (status == 201) {
                    $scope.hasBookmark = true;
                    $mdToast.show($mdToast.simple().textContent('Marcador añadido').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al añadir marcador ').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al añadir marcador ').position('bottom left').hideDelay(3000));
            });
    };

    $scope.removeBookmark = function () {
        UserService.removeTermBookmark(idUser, idTerm)
            .success(function (data, status) {
                if (status == 204) {
                    $scope.hasBookmark = false;
                    $mdToast.show($mdToast.simple().textContent('Marcador eliminado').position('bottom left').hideDelay(3000));
                } else {
                    $mdToast.show($mdToast.simple().textContent('Error al eliminar marcador ').position('bottom left').hideDelay(3000));
                }
            })
            .error(function (response) {
                $mdToast.show($mdToast.simple().textContent('Error al eliminar marcador ').position('bottom left').hideDelay(3000));
            });
    };

    $scope.removeTerm = function (term) {
        TermService.deleteTerm(term.idTerm)
            .success(function (data, status) {
                if (status == 204) {
                    $location.path('/my/terms');
                }
            });
    };

    /**
     * Información del término
     * @param type
     */
    $scope.getOriginsAndApplicatives = function (type) {

        TermService.getTermIncidences(idTerm, $scope.page.number, $scope.page.size)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.incidences = data.content;
                }
            });

        if (type == 2) { //DIMENSIÓN
            TermService.getTermDimension(idTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.typeProperties = data;
                    }
                });
        } else if (type == 3) { //INDICADOR
            TermService.getTermIndicator(idTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.typeProperties = data;
                    }
                });
            TermService.getDimensionIndicators(idTerm, $scope.page.number, $scope.page.size)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.indicatorDimensions = data.content;
                    }
                });
        } else if (type == 4) { //OBJETIVO
            TermService.getTermObjective(idTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.typeProperties = data;
                    }
                });
            TermService.getAsociatedIndicatorsFromObjective(idTerm, $scope.page.number, $scope.page.size)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.asociatedIndicators = data.content;
                    }
                });

            TermService.getObjectiveDimensions(idTerm, $scope.page.number, $scope.page.size)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.objectiveDimensions = data.content;
                    }
                });
        }
    };

    $scope.loading = true;
    TermService.getTerm($routeParams.idTerm)
        .success(function (data, status) {
            if (status == 200) {
                $scope.term = data;
                $rootScope.contentHead = data.name;
                var type = data.type.type;
                if (type == 1 || type == 6) { //CONCEPTO O FUENTE DE DATOS
                    // VOID
                } else if (type == 2 || type == 3 || type == 4) { //DIMENSIÓN, INDICADOR U OBJETIVO
                    $scope.getOriginsAndApplicatives(type);
                } else if (type == 5) { //INFORME
                    TermService.getTermInform(idTerm)
                        .success(function (data, status) {
                            if (status == 200) {
                                $scope.inform = data;
                            }
                        });

                    TermService.getInformIndicators(idTerm, $scope.page.number, $scope.page.number)
                        .success(function (data, status) {
                            if (status == 200) {
                                $scope.informIndicators = data.content;
                            }
                        });
                }
                $scope.loading = false;
            } else {
                $scope.loading = false;
            }
        })
        .error(function (response) {
            $scope.loading = false;
        });

    TermService.getDenominationsFromTerm($routeParams.idTerm, $scope.page.number, $scope.page.size)
        .success(function (data, status) {
            if (status == 200) {
                $scope.denominations = data.content;
            }
        });

    TermService.getTermScope($routeParams.idTerm)
        .success(function (data, status) {
            if (status == 200) {
                $scope.scope = data.scope;
            }
        });

    TermService.getTermCategories($routeParams.idTerm)
        .success(function (data, status) {
            if (status == 200) {
                $scope.categories = data;
            }
        });

    TermService.getExamplesFromTerm($routeParams.idTerm, 0, 20, '')
        .success(function (data, status) {
            if (status == 200) {
                $scope.examples = data.content;
            }
        });

    TermService.getExclusionsFromTerm($routeParams.idTerm, 0, 20, '')
        .success(function (data, status) {
            if (status == 200) {
                $scope.exclusions = data.content;
            }
        });

    $scope.download = function () {
        var creation = new Date($scope.term.creation);
        var day = creation.getDay();
        if (day < 10) {
            day = '0' + day;
        }
        var month = creation.getMonth();
        if (month < 10) {
            month = '0' + month;
        }
        var year = creation.getFullYear();
        var _creation = day + ' - ' + month + ' - ' + year;

        var lastUpdate = new Date($scope.term.lastUpdate);
        var day = lastUpdate.getDay();
        if (day < 10) {
            day = '0' + day;
        }
        var month = lastUpdate.getMonth();
        if (month < 10) {
            month = '0' + month;
        }
        var year = lastUpdate.getFullYear();
        var _lastUpdate = day + ' - ' + month + ' - ' + year;

        var denominations = '';
        if ($scope.denominations)
            $scope.denominations.forEach(function (entry) {
                denominations += entry.denomination + '\n';
            });
        if (denominations == '')
            denominations = '-';

        var examples = '';
        if ($scope.examples)
            $scope.examples.forEach(function (entry) {
                examples += entry.example + '\n';
            });
        if (examples == '')
            examples = '-';

        var exclusions = '';
        if ($scope.exclusions)
            $scope.exclusions.forEach(function (entry) {
                exclusions += entry.exclusion + '\n';
            });
        if (exclusions == '')
            exclusions = '-';

        if ($scope.typeProperties) {
            if ($scope.typeProperties.period)
                var period = $scope.typeProperties.period + ' días';
            else
                var period = '-';

            if ($scope.typeProperties.applicative_denomination)
                var applicative_denomination = $scope.typeProperties.applicative_denomination;
            else
                var applicative_denomination = '-';

            if ($scope.typeProperties.hierarchy)
                var hierarchy = $scope.typeProperties.hierarchy;
            else
                var hierarchy = '-';

            if ($scope.typeProperties.functionalInterpretation)
                var functionalInterpretation = $scope.typeProperties.functionalInterpretation;
            else
                var functionalInterpretation = '-';

            if ($scope.typeProperties.formula)
                var formula = $scope.typeProperties.formula;
            else
                var formula = '-';

            if ($scope.typeProperties.granularity)
                var granularity = $scope.typeProperties.granularity;
            else
                var granularity = '-';

            if ($scope.typeProperties.link)
                var link = $scope.typeProperties.link;
            else
                var link = '-';
        } else {
            var period = '-';
            var applicative_denomination = '-';
            var hierarchy = '-';
            var functionalInterpretation = '-';
            var formula = '-';
            var granularity = '-';
            var link = '-';

        }

        if ($scope.typeProperties && $scope.typeProperties.origin) {
            var originName = $scope.typeProperties.origin.name;

            var originCreation = new Date($scope.typeProperties.originCreation);
            var day = originCreation.getDay();
            if (day < 10) {
                day = '0' + day;
            }
            var month = originCreation.getMonth();
            if (month < 10) {
                month = '0' + month;
            }
            var year = originCreation.getFullYear();
            var _originCreation = day + ' - ' + month + ' - ' + year;

            var originUpdate = new Date($scope.typeProperties.originUpdate);
            var day = originUpdate.getDay();
            if (day < 10) {
                day = '0' + day;
            }
            var month = originUpdate.getMonth();
            if (month < 10) {
                month = '0' + month;
            }
            var year = originUpdate.getFullYear();
            var _originUpdate = day + ' - ' + month + ' - ' + year;
        } else {
            var originName = '';
            var _originCreation = '';
            var _originUpdate = '';
        }

        if ($scope.typeProperties && $scope.typeProperties.applicative)
            var applicativeName = $scope.typeProperties.applicative.name;
        else
            var applicativeName = '-';

        var categories = '';
        if ($scope.categories)
            $scope.categories.forEach(function (entry) {
                categories += entry.category.name;
            });
        if (categories == '')
            categories = '-';

        var indicatorDimensions = '';
        if ($scope.indicatorDimensions)
            $scope.indicatorDimensions.forEach(function (entry) {
                indicatorDimensions += entry.dimension.name + '\n';
            });
        if (indicatorDimensions == '')
            indicatorDimensions = '-';

        var objectiveDimensions = '';
        if ($scope.objectiveDimensions)
            $scope.objectiveDimensions.forEach(function (entry) {
                objectiveDimensions += entry.dimension.name + '\n';
            });
        if (objectiveDimensions == '')
            objectiveDimensions = '-';

        var asociatedIndicators = '';
        if ($scope.asociatedIndicators)
            $scope.asociatedIndicators.forEach(function (entry) {
                asociatedIndicators += entry.indicator.name + '\n';
            });
        if (asociatedIndicators == '')
            asociatedIndicators = '-';

        var informIndicators = '';
        if ($scope.informIndicators)
            $scope.informIndicators.forEach(function (entry) {
                informIndicators += entry.indicator.name + '\n';
            });
        if (informIndicators == '')
            informIndicators = '-';

        if ($scope.scope)
            var scope = $scope.scope.name;
        else
            var scope = '-';

        if ($scope.term.extendedDefinition)
            var extendedDefinition = $scope.term.extendedDefinition
        else
            var extendedDefinition = '-';

        var docDefinition = {
            content: [
                {text: $scope.term.name, style: 'header'},
                {text: ' ', style: 'header'},
                {
                    table: {
                        headerRows: 0,
                        widths: ['auto', '*'], //'auto', 100,
                        body: [
                            ['Tipo', $scope.term.type.name],
                            ['Autor', $scope.term.owner.name + ' ' + $scope.term.owner.surnames],
                            ['Fecha de creación', _creation],
                            ['Fecha de actualización', _lastUpdate],
                            ['Versión', $scope.term.version],
                            ['Descripción', $scope.term.definition],
                            ['Descripción extendida', extendedDefinition],
                            ['Denominaciones', denominations],
                            ['Ejemplos', examples],
                            ['Exclusiones', exclusions],
                            ['Ámbito', scope],
                            ['Categorías', categories],
                            ['Dimensiones del indicador', indicatorDimensions],
                            ['Dimensiones del objetivo', objectiveDimensions],
                            ['Indicadores asociados', asociatedIndicators],
                            ['Indicadores del informe', informIndicators],
                            ['Origen operacional', originName],
                            ['Inicio de uso del origen', _originCreation],
                            ['Última actualización desde el origen', _originUpdate],
                            ['Frecuencia de actualización', period],
                            ['Aplicativo analítico', applicativeName],
                            ['Denominación en el aplicativo', applicative_denomination],
                            ['Jerarquía', hierarchy],
                            ['Interpretación funcional', functionalInterpretation],
                            ['Fórmula', formula],
                            ['Granularidad', granularity],
                            ['Link', link]
                        ]
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 22,
                    bold: true,
                    alignment: 'center'
                },
                cursive: {
                    bolditalic: true,
                    alignment: 'left'
                }
            }
        };
        pdfMake.createPdf(docDefinition).open();
    };
});
