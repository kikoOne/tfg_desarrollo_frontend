/**
 * Created by jguzman on 24/05/2016.
 */

angular.module('metaDW').controller('CategoryListController', function ($scope, $rootScope, CategoryService, SearchService, $timeout) {

    $rootScope.contentHead = 'Listado de categorías';

    $scope.categoryListPage = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: ''
    }

    $scope.sortList = [
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        },
        {
            name: 'Autor',
            value: 'owner.name'
        }
    ];

    $scope.getCategories = function () {
        $scope.loading = true;
        CategoryService.getRootCategories($scope.categoryListPage.number, $scope.categoryListPage.size,
                $scope.categoryListPage.sort.property + ',' + $scope.categoryListPage.sort.direction)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.categories = data.content;
                    $scope.categoryListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.getCategories();

    $scope.selectCategory = function (idCategory) {
        if ($scope.selectedCategory && $scope.selectedCategory == idCategory) {
            $scope.selectedCategory = undefined;
            $scope.subcategories = undefined;
        } else {
            $scope.loadingSubcategories = true;
            $scope.subcategories = undefined;
            $scope.selectedCategory = idCategory;
            CategoryService.getCategoriesFromCategory(idCategory, $scope.categoryListPage.number, $scope.categoryListPage.size, '')
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.isLastSubcategoryPage = data.last;
                        $scope.loadingSubcategories = false;
                        $timeout(function () {
                            $scope.subcategories = data.content
                        }, 650);
                    } else {
                        $scope.loadingSubcategories = false;
                    }
                })
                .error(function (response) {
                    $scope.loadingSubcategories = false;
                });
        }
    };

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.$watch('categoryListPage.sort.property', function () {
        if ($scope.categories) {
            if ($scope.searchParam)
                $scope.searchRootCategories();
            else
                $scope.getCategories();
        }
    });

    $scope.$watch('categoryListPage.number', function () {
        if ($scope.categories) {
            if ($scope.searchParam)
                $scope.searchRootCategories();
            else
                $scope.getCategories();
        }
    });

    $scope.$watch('categoryListPage.size', function () {
        if ($scope.categories) {
            if ($scope.searchParam)
                $scope.searchRootCategories();
            else
                $scope.getCategories();
        }
    });

    $scope.searchRootCategories = function () {
        $scope.loading = true;
        SearchService.findCategoriesByNameAndType($scope.categoryListPage, $scope.searchParam, -1)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.categories = data.content;
                    $scope.categoryListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.$watch('searchParam', function () {
        if ($scope.categories) {
            $scope.searchRootCategories();
        }
    });

    $scope.previousPage = function () {
        $scope.categoryListPage.number--;
    };

    $scope.nextPage = function () {
        $scope.categoryListPage.number++;
    };
});
