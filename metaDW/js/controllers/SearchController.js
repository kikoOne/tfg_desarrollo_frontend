/**
 * Created by jguzman on 23/06/2016.
 */

angular.module('metaDW').controller('SearchController', function ($scope, $routeParams, SearchService, TermService, $rootScope) {

    //Se guardan las estructuras de paginación y ordenación para los diferentes tipos
    var globalSearchQuery = $routeParams.globalSearchQuery;
    // $rootScope.globalSearchQuery = '';
    $scope.seletedSort = 'name';
    $scope.pageSize = 5;
    $scope.scopePage = {
        number: 0,
        size: $scope.pageSize,
        sort: {
            property: $scope.seletedSort,
            direction: 'asc'
        },
        totalPages: ''
    };
    $scope.categoryPage = {
        number: 0,
        size: $scope.pageSize,
        sort: {
            property: $scope.seletedSort,
            direction: 'asc'
        },
        totalPages: ''
    };
    $scope.termPage = {
        number: 0,
        size: $scope.pageSize,
        sort: {
            property: $scope.seletedSort,
            direction: 'asc'
        },
        totalPages: ''
    };
    $scope.sortList = [
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        },
        {
            name: 'Autor',
            value: 'owner.name'
        }
    ];

    //Recuperar los tipo de términos
    TermService.getTermTypes()
        .success(function (data, status) {
            if (status == 200) {
                $scope.termTypes = data;
                $scope.termTypes.forEach(function (entry) {
                    $scope.toggle(entry, $scope.selected);
                });
            }
        });

    //Lógica de selectores
    $scope.scopeSelection = true;
    $scope.toggleScope = function () {
        $scope.scopeSelection = !$scope.scopeSelection;
    };
    $scope.categorySelection = true;
    $scope.toggleCategory = function () {
        $scope.categorySelection = !$scope.categorySelection;
    };
    $scope.selected = [];
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };
    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };
    $scope.termSelection = true;
    $scope.toogleTerms = function () {
        $scope.termSelection = !$scope.termSelection;
        if ($scope.termSelection) {
            $scope.selected = [];
            $scope.termTypes.forEach(function (entry) {
                $scope.toggle(entry, $scope.selected);
            });
        } else {
            $scope.selected = [];
            $scope.terms = [];
        }
    };

    //Métodos de búsquedas
    $scope.searchScopes = function (query, add) {
        if (!add)
            $scope.loading = true;
        SearchService.findScopesByName($scope.scopePage, query)
            .success(function (data, status) {
                if (status == 200) {
                    if (add) {
                        data.content.forEach(function (item) {
                            $scope.scopes.push(item);
                        });
                    } else {
                        $scope.scopes = data.content;
                        $scope.scopePage.totalPages = data.totalPages;
                        $scope.loading = false;
                    }
                }
            });
    };
    $scope.searchCategories = function (query, add) {
        if (!add)
            $scope.loading = true;
        SearchService.findCategoriesByName($scope.categoryPage, query)
            .success(function (data, status) {
                if (status == 200) {
                    if (add) {
                        data.content.forEach(function (item) {
                            $scope.categories.push(item);
                        });
                    } else {
                        $scope.categories = data.content;
                        $scope.categoryPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    }
                }
            });
    };
    $scope.searchTerms = function (query, add) {
        if (!add)
            $scope.loading = true;
        var selectedTypes = [];
        $scope.selected.forEach(function (entry) {
            selectedTypes.push(entry.type);
        });
        SearchService.findTermsByNameAndType($scope.termPage, query, selectedTypes)
            .success(function (data, status) {
                if (status == 200) {
                    if (add) {
                        data.content.forEach(function (item) {
                            $scope.terms.push(item);
                        });
                    } else {
                        $scope.terms = data.content;
                        $scope.termPage.totalPages = data.totalPages;
                        $scope.loading = false;
                    }
                }
            });
    };

    $scope.search = function (query) {
        if (query) {
            if ($scope.scopeSelection) {
                $scope.searchScopes(query, false)
            }
            if ($scope.categorySelection) {
                $scope.searchCategories(query, false);
            }
            if ($scope.selected.length > 0) {
                $scope.searchTerms(query, false);
            }
        } else {
            $scope.categories = [];
            $scope.scopes = [];
            $scope.terms = [];
        }
    };

    //Watchers a las variables
    // $scope.$watch('globalSearchQuery', function () {
    //     $scope.renewPages();
    //     $scope.search($scope.globalSearchQuery);
    // });
    $scope.$watchCollection('selected', function () {
        $scope.renewPages();
        if ($scope.selected.length > 0 && !$scope.termSelection) {
            $scope.termSelection = true;
        } else if ($scope.selected.length == 0 && $scope.termSelection) {
            $scope.termSelection = false;
        }
        if ($scope.termSelection) {
            $scope.searchTerms(globalSearchQuery, false);
        }
    });
    $scope.$watch('seletedSort', function () {
        $scope.scopePage.sort.property = $scope.seletedSort;
        $scope.categoryPage.sort.property = $scope.seletedSort;
        $scope.termPage.sort.property = $scope.seletedSort;
        $scope.renewPages();
        $scope.search(globalSearchQuery);
    });
    $scope.$watch('scopeSelection', function () {
        $scope.renewPages();
        if ($scope.scopeSelection) {
            $scope.searchScopes(globalSearchQuery, false);
        } else {
            $scope.scopes = [];
        }
    });
    $scope.$watch('categorySelection', function () {
        $scope.renewPages();
        if ($scope.categorySelection) {
            $scope.searchCategories(globalSearchQuery, false);
        } else {
            $scope.categories = [];
        }
    });
    $scope.$watch('pageSize', function () {
        $scope.scopePage.size = $scope.pageSize;
        $scope.categoryPage.size = $scope.pageSize;
        $scope.termPage.size = $scope.pageSize;
        $scope.renewPages();
        $scope.search(globalSearchQuery);
    });
    $scope.nextScopePage = function () {
        $scope.scopePage.number++;
        $scope.searchScopes(globalSearchQuery, true);
    };
    $scope.nextCategoryPage = function () {
        $scope.categoryPage.number++;
        $scope.searchCategories(globalSearchQuery, true);
    };
    $scope.nextTermPage = function () {
        $scope.termPage.number++;
        $scope.searchTerms(globalSearchQuery, true);
    };
    $scope.renewPages = function () {
        $scope.termPage.number = 0;
        $scope.categoryPage.number = 0;
        $scope.scopePage.number = 0;
    };
});
