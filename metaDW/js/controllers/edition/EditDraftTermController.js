/**
 * Created by jguzman on 20/07/2016.
 */

angular.module('metaDW').controller('EditDraftTermController', function ($scope, $rootScope, $q, $routeParams, DraftService, TermService, JWTHelper, SearchService, ScopeService, CategoryService, OperationalOriginService, AnalyticalApplicativeService, $location, $mdToast) {

    $rootScope.contentHead = "Editar término";

    $scope.newTerm = {};
    var id = $routeParams.id;
    $scope.page = { //paginado para elementos del término
        number: '0',
        size: '100'
    };

    $scope.listPage = { //paginado y orden por defecto para búsquedas
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'name',
            'direction': 'asc'
        }
    };

    $scope.selectedOriginItem = null;
    $scope.selectedApplicativeItem = null;

    $scope.selectedInformIndicators = [];
    $scope.selectedDimensions = [];
    $scope.selectedIndicators = [];
    $scope.selectedCategories = [];
    $scope.denominations = [];
    $scope.exclusions = [];
    $scope.categories = [];
    $scope.examples = [];

    //RECUPERAR EL TÉRMINO
    DraftService.getDraftTerm(id)
        .success(function (data, status) {
            if (status == 200) {
                $scope.newTerm.mainTerm = {
                    idTerm: data.idTerm,
                    name: data.name,
                    shortName: data.shortName,
                    definition: data.definition,
                    extendedDefinition: data.extendedDefinition,
                    creation: data.creation,
                    lastUpdate: data.lastUpdate,
                    type: data.type,
                    version: data.version
                };

                $scope.getTermTypes();
                $scope.getScope(data.scope);
                $scope.getCategories(data.categories);
                $scope.getDenominations(data.denominations);
                $scope.getExamples(data.examples);
                $scope.getExclusions(data.exclusions);
                $scope.getTermDetails(data.type.type);
            }
        });

    /**
     * Recuperación de característas desde histórico
     */
    $scope.getScope = function (idScope) {
        if (idScope) {
            ScopeService.getScope(idScope)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.selectedScopeItem = data;
                    }
                });
        }
    };

    $scope.getCategories = function (categories) {
        if (categories) {
            var arr = categories.split(",");
            arr.forEach(function (entry) {
                CategoryService.getCategory(entry)
                    .success(function (data, status) {
                        if (status == 200) {
                            $scope.selectedCategories.push(data);
                        }
                    });
            });
        }
    };
    //Denominations
    $scope.getDenominations = function (denominations) {
        if (denominations) {
            var arr = denominations.split(",");
            arr.forEach(function (entry) {
                $scope.denominations.push(entry);
            });
        }
    };
    //Examples
    $scope.getExamples = function (examples) {
        if (examples) {
            var arr = examples.split(",");
            arr.forEach(function (entry) {
                $scope.examples.push(entry);
            });
        }
    };
    //Exclusions
    $scope.getExclusions = function (exclusions) {
        if (exclusions) {
            var arr = exclusions.split(",");
            arr.forEach(function (entry) {
                $scope.exclusions.push(entry);
            });
        }
    };

    $scope.getTermDetails = function (termType) {
        if (termType == 2) {
            DraftService.getDraftTermDimension(id)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.setOriginsAndApplicatives(data);
                        $scope.newTerm.hierarchy = data.hierarchy;
                    }
                });
        } else if (termType == 3) {
            DraftService.getDraftTermIndicator(id)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.setOriginsAndApplicatives(data);
                        $scope.newTerm.functionalInterpretation = data.functionalInterpretation;
                        $scope.newTerm.formula = data.formula;
                        $scope.newTerm.granularity = data.granularity;
                        $scope.selectedDimensions = $scope.getDimensions(data.dimensions);
                    }
                });
        } else if (termType == 4) {
            DraftService.getDraftTermObjective(id)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.setOriginsAndApplicatives(data);
                        $scope.newTerm.functionalInterpretation = data.functionalInterpretation;
                        $scope.newTerm.formula = data.formula;
                        $scope.newTerm.granularity = data.granularity;
                        $scope.selectedDimensions = $scope.getDimensions(data.dimensions);
                        $scope.selectedIndicators = $scope.getIndicators(data.indicators);
                    }
                });
        } else if (termType == 5) {
            DraftService.getDraftTermInform(id)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.newTerm.link = data.link;
                        $scope.selectedInformIndicators = $scope.getIndicators(data.indicators);
                    }
                });
        }
    };

    $scope.setOriginsAndApplicatives = function (data) {
        if (data.origin) {
            //Recuperar origen
            OperationalOriginService.getOperationalOrigin(data.origin)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.selectedOriginItem = data;
                        $scope.newTerm.origin = data.idOrigin;
                    }
                });
            $scope.newTerm.originCreation = new Date(data.originCreation);
            $scope.newTerm.originUpdate = new Date(data.originUpdate);
            $scope.newTerm.period = data.period;
        }
        if (data.applicative) {
            //Recuperar aplicativo
            AnalyticalApplicativeService.getAnalyticalApplicative(data.applicative)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.selectedApplicativeItem = data;
                        $scope.newTerm.applicative = data.idApplicative;
                    }
                });
            $scope.newTerm.applicativeDenomination = data.applicative_denomination;
        }
    };

    $scope.getDimensions = function (dimensions) {
        var res = [];
        if (dimensions) {
            var arr = dimensions.split(",");
            arr.forEach(function (entry) {
                TermService.getTerm(entry)
                    .success(function (data, status) {
                        if (status == 200) {
                            res.push(data);
                        }
                    });
            });
        }
        return res;
    };

    $scope.getIndicators = function (indicators) {
        var res = [];
        if (indicators) {
            var arr = indicators.split(",");
            arr.forEach(function (entry) {
                TermService.getTerm(entry)
                    .success(function (data, status) {
                        if (status == 200) {
                            res.push(data);
                        }
                    });
            });
        }
        return res;
    };

    //OBTENER TIPOS DE TÉRMINOS
    $scope.getTermTypes = function () {
        TermService.getTermTypes()
            .success(function (data, status) {
                if (status == 200) {
                    $scope.termTypes = data;
                }
            });
    };

    //ACTUALIZAR TIPO DE TÉRMINO EN MD-SELECT
    $scope.$watch('newTerm.mainTerm.type.type', function () {
        if ($scope.newTerm.mainTerm && $scope.newTerm.mainTerm.type.type && $scope.termTypes) {
            $scope.termTypes.forEach(function (entry) {
                if (entry.type == $scope.newTerm.mainTerm.type.type) {
                    $scope.newTerm.mainTerm.type = entry;
                }
            });
        }
    });

    //BÚSQUEDAS
    var pendingSearch = false;
    //BUSCAR INDICADORES
    //ÁMBITOS
    $scope.findScopesByName = function () {
        SearchService.findScopesByName($scope.listPage, $scope.scopeQuery)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scopes = data.content;
                }
            });
    };
    $scope.findScopesByName();

    $scope.selectedScope = function () {
        if ($scope.selectedScopeItem) {
            $scope.newTerm.idScope = $scope.selectedScopeItem.idScope;
        }
    };

    //ORIGENES
    $scope.originQuery = '';
    $scope.findOriginByName = function () {
        SearchService.findOperationalOriginsByName($scope.listPage, $scope.originQuery)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.origins = data.content;
                }
            });
    };
    $scope.findOriginByName();

    $scope.selectedOrigin = function (value) {
        if (value && value.idOrigin) {
            $scope.newTerm.origin = value.idOrigin;
            $scope.selectedOriginItem = value;
        } else {
            $scope.newTerm.origin = null;
            $scope.selectedOriginItem = null;
        }
    };

    //CATEGORÍAS
    $scope.findCategoriesByName = function (criteria) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findCategoriesByNameAndTypes($scope.listPage, criteria, [0, 2])
                    .success(function (data, status) {
                        if (status == 200) {
                            pendingSearch = null;
                            resolve(data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    //APLICATIVOS
    $scope.findApplicativeByName = function (query) {
        SearchService.findAnalyticalApplicativesByName($scope.listPage, query)
            .then(function (response) {
                if (response.status == 200) {
                    $scope.applicatives = response.data.content;
                }
            });
    };
    $scope.findApplicativeByName('');

    $scope.selectedApplicative = function (input) {
        if (input) {
            $scope.newTerm.applicative = input.idApplicative;
        }
    };

    //BUSCAR DIMENSIONES
    $scope.findTermDimensionsByName = function (input) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findTermDimensionsByName($scope.listPage, input)
                    .then(function (response) {
                        if (response.status == 200) {
                            pendingSearch = null;
                            resolve(response.data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    //BUSCAR INDICADORES
    $scope.findTermIndicatorsByName = function (input) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findTermIndicatorsByName($scope.listPage, input)
                    .then(function (response) {
                        if (response.status == 200) {
                            pendingSearch = null;
                            resolve(response.data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    function hasAllParams() {
        if ($scope.newTerm.mainTerm.name && $scope.newTerm.mainTerm.type.type && $scope.newTerm.mainTerm.version && $scope.newTerm.mainTerm.shortName && $scope.newTerm.mainTerm.definition)
            return true;
        else
            return false;
    }

    //Publicar término
    $scope.saveFinalTerm = function () {
        $scope.updateTerm(1);
    };
    //Guardar versión termporal
    $scope.saveTemporalTerm = function () {
        $scope.updateTerm(0);
    };

    //Actualizar término
    $scope.updateTerm = function (state) {
        if (hasAllParams()) {
            $scope.newTerm.mainTerm.state = state;  //Estado final/no final
            $scope.loadData();
            /**
             * En el caso de que exista id del término se actualiza, en otro caso se crea
             */
            if ($scope.newTerm.mainTerm.idTerm) {
                TermService.updateTerm($scope.newTerm.mainTerm.idTerm, $scope.newTerm)
                    .success(function (data, status) {
                        if (status == 200) {
                            $scope.removeDraft();
                            $mdToast.show($mdToast.simple().textContent('Término actualizado').position('bottom left').hideDelay(3000));
                            $location.path('/terms/' + $scope.newTerm.mainTerm.idTerm);
                        } else {
                            $mdToast.show($mdToast.simple().textContent('Error al actualizar término').position('bottom left').hideDelay(3000));
                        }
                    })
                    .error(function (response) {
                        $mdToast.show($mdToast.simple().textContent('Error al actualizar término').position('bottom left').hideDelay(3000));
                    });
            } else {
                TermService.createTerm($scope.newTerm)
                    .success(function (data, status) {
                        if (status == 201) {
                            $scope.removeDraft();
                            $mdToast.show($mdToast.simple().textContent('Término creado').position('bottom left').hideDelay(3000));
                            $location.path('/terms/' + $scope.newTerm.mainTerm.idTerm);
                        } else {
                            $mdToast.show($mdToast.simple().textContent('Error al crear término').position('bottom left').hideDelay(3000));
                        }
                    })
                    .error(function (response) {
                        $mdToast.show($mdToast.simple().textContent('Error al crear término').position('bottom left').hideDelay(3000));
                    });
            }
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    }

    $scope.saveDraft = function () {
        if (hasAllParams()) {
            $scope.loadData();
            DraftService.updateDraftTerm(id, $scope.newTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Borrador actualizado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al actualizar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al actualizar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámetros obligatorios').position('bottom left').hideDelay(3000));
        }
    }

    $scope.loadData = function () {
        $scope.newTerm.mainTerm.owner = JWTHelper.getTokenId();
        if ($scope.selectedScopeItem) {
            $scope.newTerm.idScope = $scope.selectedScopeItem.idScope; //Ámbito
        }
        /*Categorías*/
        if ($scope.selectedCategories) {
            $scope.newTerm.categories = [];
            $scope.selectedCategories.forEach(function (item) {
                $scope.newTerm.categories.push(item.idCategory);
            });
        }
        //Denominaciones
        if ($scope.denominations && $scope.denominations.length > 0) {
            $scope.newTerm.denominations = [];
            $scope.denominations.forEach(function (entry) {
                var item = {
                    idTerm: $scope.newTerm.mainTerm.idTerm,
                    denomination: entry,
                    idDenomination: null
                };
                $scope.newTerm.denominations.push(item);
            });
        }
        $scope.newTerm.examples = $scope.examples;  // Ejemplos
        $scope.newTerm.exclusions = $scope.exclusions;  // Exclusions

        var type = $scope.newTerm.mainTerm.type.type;    // Tipo
        if (type == 2 || type == 3 || type == 4) {
            if ($scope.selectedOriginItem && $scope.selectedOriginItem.idOrigin) {
                $scope.newTerm.origin = $scope.selectedOriginItem.idOrigin;
            }
        }

        if (type == 3) {    // DIMENSIONES
            if ($scope.selectedDimensions) {
                $scope.newTerm.indicatorsDimensions = [];
                $scope.selectedDimensions.forEach(function (item) {
                    $scope.newTerm.indicatorsDimensions.push(item.idTerm);
                });
            }
        }

        if (type == 4) {    // OBJETIVO
            if ($scope.selectedIndicators) {
                $scope.newTerm.asociatedIndicators = [];
                $scope.selectedIndicators.forEach(function (item) {
                    $scope.newTerm.asociatedIndicators.push(item.idTerm);
                });
            }

            if ($scope.selectedDimensions) {
                $scope.newTerm.objectivesDimensions = [];
                $scope.selectedDimensions.forEach(function (item) {
                    $scope.newTerm.objectivesDimensions.push({
                        dimension: item.idTerm,
                        objective: $scope.newTerm.mainTerm.idTerm,
                        additivity: true
                    });
                });
            }
        }

        if (type == 5) {    // INFORMES
            if ($scope.selectedInformIndicators) {
                $scope.newTerm.informIndicators = [];
                $scope.selectedInformIndicators.forEach(function (item) {
                    $scope.newTerm.informIndicators.push(item.idTerm);
                });
            }
        }
    };

    $scope.removeDraft = function () {
        DraftService.removeDraftTerm(id)
            .success(function (data, status) {
                if (status == 204) {
                    console.log('deleted');
                }
            });
    }
});
