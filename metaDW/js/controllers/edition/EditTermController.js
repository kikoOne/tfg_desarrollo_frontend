/**
 * Created by jguzman on 20/07/2016.
 */

angular.module('metaDW').controller('EditTermController', function ($rootScope, $scope, $q, $routeParams, HistoryService, TermService, JWTHelper, SearchService, ScopeService, CategoryService, OperationalOriginService, AnalyticalApplicativeService, DraftService, $location, $mdToast) {

    $rootScope.contentHead = "Editar término";

    $scope.newTerm = {};
    var idTerm = $routeParams.idTerm;
    var idVersion = $routeParams.idVersion;
    $scope.page = { //paginado para elementos del término
        number: '0',
        size: '100'
    };

    $scope.listPage = { //paginado y orden por defecto para búsquedas
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'name',
            'direction': 'asc'
        }
    };

    $scope.selectedOriginItem = null;
    $scope.selectedApplicativeItem = null;

    $scope.selectedInformIndicators = [];
    $scope.selectedDimensions = [];
    $scope.selectedIndicators = [];
    $scope.selectedCategories = [];
    $scope.denominations = [];
    $scope.exclusions = [];
    $scope.categories = [];
    $scope.examples = [];

    //RECUPERAR EL TÉRMINO
    if (idVersion) {   //si es la edición de una versión
        HistoryService.getTermHistoryVersion(idTerm, idVersion)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.newTerm.mainTerm = {
                        idTerm: data.idTerm,
                        name: data.name,
                        shortName: data.shortName,
                        definition: data.definition,
                        extendedDefinition: data.extendedDefinition,
                        creation: data.creation,
                        lastUpdate: data.lastUpdate,
                        type: data.type
                    };
                    TermService.getTerm(idTerm)
                        .success(function (data, status) {
                            if (status == 200) {
                                var version = data.version;
                                var newVersion = parseInt(version.substr(version.indexOf(".") + 1)) + 1;
                                $scope.newTerm.mainTerm.version = version.substr(0, parseInt(version.substr(version.indexOf(".") - 1))) + "." + newVersion;
                            }
                        });
                    $scope.getTermTypes();
                    $scope.getScope(data.scope);
                    $scope.getCategories(data.categories);
                    $scope.getDenominations(data.denominations);
                    $scope.getExamples(data.examples);
                    $scope.getExclusions(data.exclusions);
                    $scope.getTermDetails(data.type.type);
                }
            });
    } else {    //si es la edición del término activo
        TermService.getTerm(idTerm)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.newTerm.mainTerm = data;
                    var version = $scope.newTerm.mainTerm.version;
                    var newVersion = parseInt(version.substr(version.indexOf(".") + 1)) + 1;
                    $scope.newTerm.mainTerm.version = version.substr(0, parseInt(version.substr(version.indexOf(".") - 1))) + "." + newVersion;
                    $scope.getTermTypes();
                    $scope.getCommon();
                    var type = data.type.type;
                    if (type == 1 || type == 6) { //CONCEPTO O FUENTE DE DATOS
                        // VOID
                    } else if (type == 2 || type == 3 || type == 4) { //DIMENSIÓN, INDICADOR U OBJETIVO
                        $scope.getOriginsAndApplicatives(type);
                    } else if (type == 5) { //INFORME
                        TermService.getTermInform(idTerm)
                            .success(function (data, status) {
                                if (status == 200) {
                                    $scope.newTerm.link = data.link;
                                }
                            });

                        TermService.getInformIndicators(idTerm, $scope.page.number, $scope.page.number)
                            .success(function (data, status) {
                                if (status == 200) {
                                    data.content.forEach(function (entry) {
                                        $scope.selectedInformIndicators.push(entry.indicator);
                                    });
                                }
                            });
                    }
                }
            });
    }

    /**
     * Recuperación de característas desde histórico
     */
    $scope.getScope = function (idScope) {
        if (idScope) {
            ScopeService.getScope(idScope)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.selectedScopeItem = data;
                    }
                });
        }
    };

    $scope.getCategories = function (categories) {
        if (categories) {
            var arr = categories.split(",");
            arr.forEach(function (entry) {
                CategoryService.getCategory(entry)
                    .success(function (data, status) {
                        if (status == 200) {
                            $scope.selectedCategories.push(data);
                        }
                    });
            });
        }
    };
    //Denominations
    $scope.getDenominations = function (denominations) {
        if (denominations) {
            var arr = denominations.split(",");
            arr.forEach(function (entry) {
                $scope.denominations.push(entry);
            });
        }
    };
    //Examples
    $scope.getExamples = function (examples) {
        if (examples) {
            var arr = examples.split(",");
            arr.forEach(function (entry) {
                $scope.examples.push(entry);
            });
        }
    };
    //Exclusions
    $scope.getExclusions = function (exclusions) {
        if (exclusions) {
            var arr = exclusions.split(",");
            arr.forEach(function (entry) {
                $scope.exclusions.push(entry);
            });
        }
    };

    $scope.getTermDetails = function (termType) {
        if (termType == 2) {
            HistoryService.getDimensionHistoryVersion(idVersion)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.setOriginsAndApplicatives(data);
                        $scope.newTerm.hierarchy = data.hierarchy;
                    }
                });
        } else if (termType == 3) {
            HistoryService.getIndicatorHistoryVersion(idVersion)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.setOriginsAndApplicatives(data);
                        $scope.newTerm.functionalInterpretation = data.functionalInterpretation;
                        $scope.newTerm.formula = data.formula;
                        $scope.newTerm.granularity = data.granularity;
                        $scope.selectedDimensions = $scope.getDimensions(data.dimensions);
                    }
                });
        } else if (termType == 4) {
            HistoryService.getObjectiveHistoryVersion(idVersion)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.setOriginsAndApplicatives(data);
                        $scope.newTerm.functionalInterpretation = data.functionalInterpretation;
                        $scope.newTerm.formula = data.formula;
                        $scope.newTerm.granularity = data.granularity;
                        $scope.selectedDimensions = $scope.getDimensions(data.dimensions);
                        $scope.selectedIndicators = $scope.getIndicators(data.indicators);
                    }
                });
        } else if (termType == 5) {
            HistoryService.getInformHistoryVersion(idVersion)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.newTerm.link = data.link;
                        $scope.selectedInformIndicators = $scope.getIndicators(data.indicators);
                    }
                });
        }
    };

    $scope.setOriginsAndApplicatives = function (data) {
        if (data.origin) {
            //Recuperar origen
            OperationalOriginService.getOperationalOrigin(data.origin)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.selectedOriginItem = data;
                        $scope.newTerm.origin = data.idOrigin;
                    }
                });
            $scope.newTerm.originCreation = new Date(data.originCreation);
            $scope.newTerm.originUpdate = new Date(data.originUpdate);
            $scope.newTerm.period = data.period;
        }
        if (data.applicative) {
            //Recuperar aplicativo
            AnalyticalApplicativeService.getAnalyticalApplicative(data.applicative)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.selectedApplicativeItem = data;
                        $scope.newTerm.applicative = data.idApplicative;
                    }
                });
            $scope.newTerm.applicativeDenomination = data.applicative_denomination;
        }
    };

    $scope.getDimensions = function (dimensions) {
        var res = [];
        if (dimensions) {
            var arr = dimensions.split(",");
            arr.forEach(function (entry) {
                TermService.getTerm(entry)
                    .success(function (data, status) {
                        if (status == 200) {
                            res.push(data);
                        }
                    });
            });
        }
        return res;
    };

    $scope.getIndicators = function (indicators) {
        var res = [];
        if (indicators) {
            var arr = indicators.split(",");
            arr.forEach(function (entry) {
                TermService.getTerm(entry)
                    .success(function (data, status) {
                        if (status == 200) {
                            res.push(data);
                        }
                    });
            });
        }
        return res;
    };

    /**
     * Recuperación de características desde edición
     */
    //Parte común
    $scope.getCommon = function () {
        TermService.getTermScope($routeParams.idTerm)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.selectedScopeItem = data.scope;
                }
            });

        TermService.getTermCategories($routeParams.idTerm)
            .success(function (data, status) {
                if (status == 200) {
                    data.forEach(function (entry) {
                        $scope.selectedCategories.push(entry.category);
                    });
                }
            });

        TermService.getDenominationsFromTerm($routeParams.idTerm, $scope.page.number, $scope.page.size)
            .success(function (data, status) {
                if (status == 200) {
                    data.content.forEach(function (entry) {
                        $scope.denominations.push(entry.denomination);
                    });
                }
            });

        TermService.getExamplesFromTerm($routeParams.idTerm, 0, 20, '')
            .success(function (data, status) {
                if (status == 200) {
                    data.content.forEach(function (entry) {
                        $scope.examples.push(entry.example);
                    });
                }
            });

        TermService.getExclusionsFromTerm($routeParams.idTerm, 0, 20, '')
            .success(function (data, status) {
                if (status == 200) {
                    data.content.forEach(function (entry) {
                        $scope.exclusions.push(entry.exclusion);
                    });
                }
            });
    }

    //OBTENER TIPOS DE TÉRMINOS
    $scope.getTermTypes = function () {
        TermService.getTermTypes()
            .success(function (data, status) {
                if (status == 200) {
                    $scope.termTypes = data;
                }
            });
    };

    //Parte de orígenes y aplicativos
    $scope.getOriginsAndApplicatives = function (type) {
        if (type == 2) { //DIMENSIÓN
            TermService.getTermDimension(idTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        //Orígen operacional
                        if (data.origin) {
                            $scope.selectedOriginItem = data.origin;
                            $scope.newTerm.origin = data.origin.idOrigin;
                            $scope.newTerm.originCreation = new Date(data.originCreation);
                            $scope.newTerm.originUpdate = new Date(data.originUpdate);
                            $scope.newTerm.period = data.period;
                        } else {
                            $scope.selectedOriginItem = undefined;
                        }
                        //Aplicativo analítico
                        if (data.applicative) {
                            $scope.selectedApplicativeItem = data.applicative;
                            $scope.newTerm.applicative = data.applicative.idApplicative;
                            $scope.newTerm.applicativeDenomination = data.applicative_denomination;
                        } else {
                            $scope.selectedApplicativeItem = undefined;
                        }
                        //Otros
                        $scope.newTerm.hierarchy = data.hierarchy;
                    }
                });
        } else if (type == 3) { //INDICADOR
            TermService.getTermIndicator(idTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        //Orígen operacional
                        if (data.origin) {
                            $scope.selectedOriginItem = data.origin;
                            $scope.newTerm.origin = data.origin.idOrigin;
                            $scope.newTerm.originCreation = new Date(data.originCreation);
                            $scope.newTerm.originUpdate = new Date(data.originUpdate);
                            $scope.newTerm.period = data.period;
                        } else {
                            $scope.selectedOriginItem = undefined;
                        }
                        //Aplicativo analítico
                        if (data.applicative) {
                            $scope.selectedApplicativeItem = data.applicative;
                            $scope.newTerm.applicative = data.applicative.idApplicative;
                            $scope.newTerm.applicativeDenomination = data.applicative_denomination;
                        } else {
                            $scope.selectedApplicativeItem = undefined;
                        }
                        //Relaciones con otros términos
                        $scope.newTerm.formula = data.formula;
                        $scope.newTerm.functionalInterpretation = data.functionalInterpretation;
                        $scope.newTerm.granularity = data.granularity;
                    }
                });
            TermService.getDimensionIndicators(idTerm, $scope.page.number, $scope.page.size)
                .success(function (data, status) {
                    if (status == 200) {
                        data.content.forEach(function (entry) {
                            $scope.selectedDimensions.push(entry.dimension);
                        });
                    }
                });
        } else if (type == 4) { //OBJETIVO
            TermService.getTermObjective(idTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        //Orígen operacional
                        if (data.origin) {
                            $scope.selectedOriginItem = data.origin;
                            $scope.newTerm.origin = data.origin.idOrigin;
                            $scope.newTerm.originCreation = new Date(data.originCreation);
                            $scope.newTerm.originUpdate = new Date(data.originUpdate);
                            $scope.newTerm.period = data.period;
                        } else {
                            console.log('no origin');
                            $scope.selectedOriginItem = null;
                            $scope.originQuery = '';
                        }
                        //Aplicativo analítico
                        if (data.applicative) {
                            $scope.selectedApplicativeItem = data.applicative;
                            $scope.newTerm.applicative = data.applicative.idApplicative;
                            $scope.newTerm.applicativeDenomination = data.applicative_denomination;
                        }
                        //Relaciones con otros términos
                        $scope.newTerm.formula = data.formula;
                        $scope.newTerm.functionalInterpretation = data.functionalInterpretation;
                        $scope.newTerm.granularity = data.granularity;
                    }
                });
            TermService.getAsociatedIndicatorsFromObjective(idTerm, $scope.page.number, $scope.page.size)
                .success(function (data, status) {
                    if (status == 200) {
                        data.content.forEach(function (entry) {
                            $scope.selectedIndicators.push(entry.indicator);
                        });
                    }
                });

            TermService.getObjectiveDimensions(idTerm, $scope.page.number, $scope.page.size)
                .success(function (data, status) {
                    if (status == 200) {
                        data.content.forEach(function (entry) {
                            $scope.selectedDimensions.push(entry.dimension);
                        });
                    }
                });
        }
    };

    //ACTUALIZAR TIPO DE TÉRMINO EN MD-SELECT
    $scope.$watch('newTerm.mainTerm.type.type', function () {
        if ($scope.newTerm.mainTerm && $scope.newTerm.mainTerm.type.type && $scope.termTypes) {
            $scope.termTypes.forEach(function (entry) {
                if (entry.type == $scope.newTerm.mainTerm.type.type) {
                    $scope.newTerm.mainTerm.type = entry;
                }
            });
        }
    });

    //BÚSQUEDAS
    var pendingSearch = false;
    //BUSCAR INDICADORES
    //ÁMBITOS
    $scope.findScopesByName = function () {
        SearchService.findScopesByName($scope.listPage, $scope.scopeQuery)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scopes = data.content;
                }
            });
    };
    $scope.findScopesByName();

    $scope.selectedScope = function () {
        if ($scope.selectedScopeItem) {
            $scope.newTerm.idScope = $scope.selectedScopeItem.idScope;
        }
    };

    //ORIGENES
    $scope.originQuery = '';
    $scope.findOriginByName = function () {
        SearchService.findOperationalOriginsByName($scope.listPage, $scope.originQuery)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.origins = data.content;
                }
            });
    };
    $scope.findOriginByName();

    $scope.selectedOrigin = function (value) {
        if (value && value.idOrigin) {
            $scope.newTerm.origin = value.idOrigin;
            $scope.selectedOriginItem = value;
        } else {
            $scope.newTerm.origin = null;
            $scope.selectedOriginItem = null;
        }
    };

    //CATEGORÍAS
    $scope.findCategoriesByName = function (criteria) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findCategoriesByNameAndTypes($scope.listPage, criteria, [0, 2])
                    .success(function (data, status) {
                        if (status == 200) {
                            pendingSearch = null;
                            resolve(data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    //APLICATIVOS
    $scope.findApplicativeByName = function (query) {
        SearchService.findAnalyticalApplicativesByName($scope.listPage, query)
            .then(function (response) {
                if (response.status == 200) {
                    $scope.applicatives = response.data.content;
                }
            });
    };
    $scope.findApplicativeByName('');

    $scope.selectedApplicative = function (input) {
        if (input) {
            $scope.newTerm.applicative = input.idApplicative;
        }
    };

    //BUSCAR DIMENSIONES
    $scope.findTermDimensionsByName = function (input) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findTermDimensionsByName($scope.listPage, input)
                    .then(function (response) {
                        if (response.status == 200) {
                            pendingSearch = null;
                            resolve(response.data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    //BUSCAR INDICADORES
    $scope.findTermIndicatorsByName = function (input) {
        if (!pendingSearch) {
            return pendingSearch = $q(function (resolve, reject) {
                SearchService.findTermIndicatorsByName($scope.listPage, input)
                    .then(function (response) {
                        if (response.status == 200) {
                            pendingSearch = null;
                            resolve(response.data.content);
                        }
                    });
            });
        }
        return pendingSearch;
    };

    function hasAllParams() {
        if ($scope.newTerm.mainTerm.name && $scope.newTerm.mainTerm.type.type && $scope.newTerm.mainTerm.version && $scope.newTerm.mainTerm.shortName && $scope.newTerm.mainTerm.definition)
            return true;
        else
            return false;
    }

    //Publicar término
    $scope.saveFinalTerm = function () {
        $scope.updateTerm(1);
    };
    //Guardar versión termporal
    $scope.saveTemporalTerm = function () {
        $scope.updateTerm(0);
    };

    $scope.saveDraft = function () {
        if (hasAllParams()) {
            $scope.setTermData();
            DraftService.addDraftTerm($scope.newTerm)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámetros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    //Actualizar término
    $scope.updateTerm = function (state) {
        if (hasAllParams()) {
            $scope.newTerm.mainTerm.state = state;  //Estado final/no final
            $scope.setTermData();

            TermService.updateTerm(idTerm, $scope.newTerm)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Término actualizado').position('bottom left').hideDelay(3000));
                        $location.path('/terms/' + $scope.newTerm.mainTerm.idTerm);
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error en actualización').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en actualización').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámetros obligatorios').position('bottom left').hideDelay(3000));
        }
    }

    $scope.setTermData = function () {
        $scope.newTerm.mainTerm.owner = JWTHelper.getTokenId();
        if ($scope.selectedScopeItem) {
            $scope.newTerm.idScope = $scope.selectedScopeItem.idScope; //Ámbito
        }
        /*Categorías*/
        if ($scope.selectedCategories) {
            $scope.newTerm.categories = [];
            $scope.selectedCategories.forEach(function (item) {
                $scope.newTerm.categories.push(item.idCategory);
            });
        }
        //Denominaciones
        if ($scope.denominations && $scope.denominations.length > 0) {
            $scope.newTerm.denominations = [];
            $scope.denominations.forEach(function (entry) {
                var item = {
                    idTerm: idTerm,
                    denomination: entry,
                    idDenomination: null
                };
                $scope.newTerm.denominations.push(item);
            });
        }
        $scope.newTerm.examples = $scope.examples;  //Ejemplos
        $scope.newTerm.exclusions = $scope.exclusions;  //Exclusions

        var type = $scope.newTerm.mainTerm.type.type;    //tipo
        if (type == 2 || type == 3 || type == 4) {
            if ($scope.selectedOriginItem && $scope.selectedOriginItem.idOrigin) {
                $scope.newTerm.origin = $scope.selectedOriginItem.idOrigin;
            }
        }
        //Dimensiones
        if (type == 3) {
            if ($scope.selectedDimensions) {
                $scope.newTerm.indicatorsDimensions = [];
                $scope.selectedDimensions.forEach(function (item) {
                    $scope.newTerm.indicatorsDimensions.push(item.idTerm);
                });
            }
        }

        if (type == 4) {
            if ($scope.selectedIndicators) {
                $scope.newTerm.asociatedIndicators = [];
                $scope.selectedIndicators.forEach(function (item) {
                    $scope.newTerm.asociatedIndicators.push(item.idTerm);
                });
            }

            if ($scope.selectedDimensions) {
                $scope.newTerm.objectivesDimensions = [];
                $scope.selectedDimensions.forEach(function (item) {
                    $scope.newTerm.objectivesDimensions.push({
                        dimension: item.idTerm,
                        objective: $scope.newTerm.mainTerm.idTerm,
                        additivity: true
                    });
                });
            }
        }

        if (type == 5) {
            if ($scope.selectedInformIndicators) {
                $scope.newTerm.informIndicators = [];
                $scope.selectedInformIndicators.forEach(function (item) {
                    $scope.newTerm.informIndicators.push(item.idTerm);
                });
            }
        }
    };
});
