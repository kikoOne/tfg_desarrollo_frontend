/**
 * Created by jguzman on 19/07/2016.
 */

angular.module('metaDW').controller('EditDraftCategoryController', function ($rootScope, $scope, $routeParams, CategoryService, DraftService, JWTHelper, $location, $mdToast, SearchService) {

    $rootScope.contentHead = "Editar categoría";

    $scope.categoryQuery = '';

    $scope.listPage = { //paginado y orden por defecto
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'name',
            'direction': 'asc'
        }
    };

    DraftService.getDraftCategory($routeParams.id)
        .success(function (data, status) {
            if (status == 200) {
                $scope.newCategory = data;
                $scope.getParentCategory(data.parent);
            }
        });

    $scope.getParentCategory = function (idParent) {
        CategoryService.getCategory(idParent)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.selectedCategoryItem = data;
                    $scope.newCategory.idParent = data.idCategory;
                }
            });
    };

    $scope.findCategoriesByName = function () {
        SearchService.findCategoriesByNameAndTypes($scope.listPage, $scope.categoryQuery, [-1, 0, 1])
            .success(function (data, status) {
                if (status == 200) {
                    $scope.categories = data.content;
                }
            });
    };

    $scope.findCategoriesByName();

    $scope.publishCategory = function () {
        $scope.updateCategory(1);
    };

    $scope.saveTemporalCategory = function () {
        $scope.updateCategory(0);
    };

    $scope.updateCategory = function (state) {
        if ($scope.newCategory.name && $scope.newCategory.version) {
            $scope.newCategory.state = state;
            $scope.newCategory.owner = JWTHelper.getTokenId();
            if ($scope.newCategory.idParent) {
                $scope.newCategory.type = 0;
            } else {
                $scope.newCategory.type = -1;
            }
            if ($scope.newCategory.idCategory) {
                CategoryService.updateCategory($routeParams.idCategory, $scope.newCategory)
                    .success(function (data, status) {
                        if (status == 200) {
                            $mdToast.show($mdToast.simple().textContent('Categoría actualizada').position('bottom left').hideDelay(3000));
                            $scope.removeDraft();
                            $location.path('/categories/' + $scope.newCategory.idCategory);
                        } else {
                            $mdToast.show($mdToast.simple().textContent('Error al actualizar categoría').position('bottom left').hideDelay(3000));
                        }
                    })
                    .error(function (response) {
                        $mdToast.show($mdToast.simple().textContent('Error al actualizar categoría').position('bottom left').hideDelay(3000));
                    });
            } else {
                CategoryService.createCategory($scope.newCategory)
                    .success(function (data, status) {
                        if (status == 201) {
                            $mdToast.show($mdToast.simple().textContent('Categoría creada').position('bottom left').hideDelay(3000));
                            $scope.removeDraft();
                            $location.path('/categories/' + $scope.newCategory.idCategory);
                        } else {
                            $mdToast.show($mdToast.simple().textContent('Error al crear categoría').position('bottom left').hideDelay(3000));
                        }
                    })
                    .error(function (response) {
                        $mdToast.show($mdToast.simple().textContent('Error al crear categoría').position('bottom left').hideDelay(3000));
                    });
            }
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.saveDraft = function () {
        if ($scope.newCategory.name && $scope.newCategory.version) {
            $scope.newCategory.owner = JWTHelper.getTokenId();
            if ($scope.newCategory.idParent) {
                $scope.newCategory.type = 0;
            } else {
                $scope.newCategory.type = -1;
            }
            DraftService.updateDraftCategory($routeParams.id, $scope.newCategory)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.removeDraft = function () {
        DraftService.removeDraftCategory($routeParams.id)
            .success(function (data, status) {
                if (status == 204) {
                }
            });
    }
});
