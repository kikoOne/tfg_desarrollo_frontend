/**
 * Created by jguzman on 05/07/2016.
 */

angular.module('metaDW').controller('EditScopeController', function ($rootScope, $scope, ScopeService, HistoryService, $routeParams, JWTHelper, DraftService, $location, $mdToast) {

    $rootScope.contentHead = "Editar ámbito";

    if ($routeParams.idVersion) {
        HistoryService.getScopeVersion($routeParams.idScope, $routeParams.idVersion)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.newScope = data;
                    ScopeService.getScope($routeParams.idScope)
                        .success(function (data, status) {
                            if (status == 200) {
                                var version = data.version;
                                var newVersion = parseInt(version.substr(version.indexOf(".") + 1)) + 1;
                                $scope.newScope.version = version.substr(0, parseInt(version.substr(version.indexOf(".") - 1))) + "." + newVersion;
                            }
                        });
                }
            });
    } else {
        ScopeService.getScope($routeParams.idScope)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.newScope = data;
                    var version = $scope.newScope.version;
                    var newVersion = parseInt(version.substr(version.indexOf(".") + 1)) + 1;
                    $scope.newScope.version = version.substr(0, parseInt(version.substr(version.indexOf(".") - 1))) + "." + newVersion;
                }
            });
    }

    $scope.publishScope = function () {
        $scope.updateScope(1);
    };

    $scope.saveTemporalScope = function () {
        $scope.updateScope(0);
    };

    $scope.updateScope = function (state) {
        if ($scope.newScope.name && $scope.newScope.version) {
            $scope.newScope.state = state;
            $scope.newScope.owner = JWTHelper.getTokenId();
            ScopeService.updateScope($routeParams.idScope, $scope.newScope)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Ámbito actualizado').position('bottom left').hideDelay(3000));
                        $location.path('/scopes/' + $scope.newScope.idScope);
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error en actualización').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error en actualización').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámetros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.saveDraft = function () {
        if ($scope.newScope.name && $scope.newScope.version) {
            $scope.newScope.owner = JWTHelper.getTokenId();
            DraftService.addDraftScope($scope.newScope)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámetros obligatorios').position('bottom left').hideDelay(3000));
        }
    };
});
