/**
 * Created by jguzman on 19/07/2016.
 */

angular.module('metaDW').controller('EditCategoryController', function ($rootScope, $scope, $routeParams, CategoryService, HistoryService, JWTHelper, DraftService, $location, $mdToast, SearchService) {

    $rootScope.contentHead = "Editar categoría";

    $scope.categoryQuery = '';

    $scope.listPage = { //paginado y orden por defecto
        'number': 0,
        'size': 10,
        'sort': {
            'property': 'name',
            'direction': 'asc'
        }
    };

    if ($routeParams.idVersion) {
        HistoryService.getCategoryVersion($routeParams.idCategory, $routeParams.idVersion)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.newCategory = data;
                    CategoryService.getCategory($routeParams.idCategory)
                        .success(function (data, status) {
                            if (status == 200) {
                                var version = data.version;
                                var newVersion = parseInt(version.substr(version.indexOf(".") + 1)) + 1;
                                $scope.newCategory.version = version.substr(0, parseInt(version.substr(version.indexOf(".") - 1))) + "." + newVersion;
                                $scope.getParentCategory();
                            }
                        });
                }
            });
    } else {
        CategoryService.getCategory($routeParams.idCategory)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.newCategory = data;
                    var version = $scope.newCategory.version;
                    var newVersion = parseInt(version.substr(version.indexOf(".") + 1)) + 1;
                    $scope.newCategory.version = version.substr(0, parseInt(version.substr(version.indexOf(".") - 1))) + "." + newVersion;
                    $scope.getParentCategory();
                }
            });
    }

    $scope.getParentCategory = function () {
        CategoryService.getParentCategory($routeParams.idCategory)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.selectedCategoryItem = data;
                    $scope.newCategory.idParent = data.idCategory;
                }
            });
    };

    $scope.findCategoriesByName = function () {
        SearchService.findCategoriesByNameAndTypes($scope.listPage, $scope.categoryQuery, [-1, 0, 1])
            .success(function (data, status) {
                if (status == 200) {
                    $scope.categories = data.content;
                }
            });
    };

    $scope.findCategoriesByName();

    $scope.publishCategory = function () {
        $scope.updateCategory(1);
    };

    $scope.saveTemporalCategory = function () {
        $scope.updateCategory(0);
    };

    $scope.updateCategory = function (state) {
        if ($scope.newCategory.name && $scope.newCategory.version) {
            $scope.newCategory.state = state;
            $scope.newCategory.owner = JWTHelper.getTokenId();
            if ($scope.newCategory.idParent) {
                $scope.newCategory.type = 0;
            } else {
                $scope.newCategory.type = -1;
            }
            CategoryService.updateCategory($routeParams.idCategory, $scope.newCategory)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Categoría actualizada').position('bottom left').hideDelay(3000));
                        $location.path('/categories/' + $scope.newCategory.idCategory);
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al crear categoría').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al crear categoría').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.saveDraft = function () {
        if ($scope.newCategory.name && $scope.newCategory.version) {
            $scope.newCategory.owner = JWTHelper.getTokenId();
            DraftService.addDraftCategory($scope.newCategory)
                .success(function (data, status) {
                    if (status == 201) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };
});
