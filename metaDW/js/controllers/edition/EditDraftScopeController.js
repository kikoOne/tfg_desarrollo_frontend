/**
 * Created by jguzman on 05/07/2016.
 */

angular.module('metaDW').controller('EditDraftScopeController', function ($scope, $rootScope, ScopeService, DraftService, $routeParams, JWTHelper, $location, $mdToast) {

    $rootScope.contentHead = "Editar ámbito";

    DraftService.getDraftScope($routeParams.id)
        .success(function (data, status) {
            if (status == 200) {
                $scope.newScope = data;
            }
        });

    $scope.publishScope = function () {
        $scope.updateScope(1);
    };

    $scope.saveTemporalScope = function () {
        $scope.updateScope(0);
    };

    $scope.updateScope = function (state) {
        if ($scope.newScope.name && $scope.newScope.version) {
            $scope.newScope.state = state;
            $scope.newScope.owner = JWTHelper.getTokenId();

            if ($scope.newScope.idScope) {
                ScopeService.updateScope($routeParams.idScope, $scope.newScope)
                    .success(function (data, status) {
                        if (status == 200) {
                            $mdToast.show($mdToast.simple().textContent('Ámbito actualizado').position('bottom left').hideDelay(3000));
                            $scope.removeDraft();
                            $location.path('/scopes/' + $scope.newScope.idScope);
                        } else {
                            $mdToast.show($mdToast.simple().textContent('Error al actualizar ámbito').position('bottom left').hideDelay(3000));
                        }
                    })
                    .error(function (response) {
                        $mdToast.show($mdToast.simple().textContent('Error al actualizar ámbito').position('bottom left').hideDelay(3000));
                    });
            } else {
                ScopeService.createScope($scope.newScope)
                    .success(function (data, status) {
                        if (status == 201) {
                            $mdToast.show($mdToast.simple().textContent('Ámbito creado').position('bottom left').hideDelay(3000));
                            $scope.removeDraft();
                            $location.path('/scopes/' + $scope.newScope.idScope);
                        } else {
                            $mdToast.show($mdToast.simple().textContent('Error al crear ámbito').position('bottom left').hideDelay(3000));
                        }
                    })
                    .error(function (response) {
                        $mdToast.show($mdToast.simple().textContent('Error al crear ámbito').position('bottom left').hideDelay(3000));
                    });
            }
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.saveDraft = function () {
        if($scope.newScope.name && $scope.newScope.version) {
            $scope.newScope.owner = JWTHelper.getTokenId();
            DraftService.updateDraftScope($routeParams.id, $scope.newScope)
                .success(function (data, status) {
                    if (status == 200) {
                        $mdToast.show($mdToast.simple().textContent('Borrador guardado').position('bottom left').hideDelay(3000));
                        $location.path('/my/drafts');
                    } else {
                        $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                    }
                })
                .error(function (response) {
                    $mdToast.show($mdToast.simple().textContent('Error al guardar borrador').position('bottom left').hideDelay(3000));
                });
        } else {
            $mdToast.show($mdToast.simple().textContent('Faltan parámentros obligatorios').position('bottom left').hideDelay(3000));
        }
    };

    $scope.removeDraft = function () {
        DraftService.removeDraftScope($routeParams.id)
            .success(function (data, status) {
                if (status == 204) {
                    console.log('deleted');
                }
            });
    }
});
