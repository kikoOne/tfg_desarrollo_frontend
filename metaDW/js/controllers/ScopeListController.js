/**
 * Created by jguzman on 08/06/2016.
 */

angular.module('metaDW').controller('ScopeListController', function ($scope, $rootScope, ScopeService, SearchService) {

    $rootScope.contentHead = 'Listado de ámbitos';

    $scope.scopeListPage = { //paginado y orden por defecto
        number: 0,
        size: 20,
        sort: {
            property: 'name',
            direction: 'asc'
        },
        totalPages: ''
    }

    $scope.sortList = [
        {
            name: 'Nombre',
            value: 'name'
        },
        {
            name: 'Fecha de creación',
            value: 'creation'
        },
        {
            name: 'Autor',
            value: 'owner.name'
        }
    ];

    $scope.getScopes = function () {
        $scope.loading = true;
        ScopeService.getScopes($scope.scopeListPage.number, $scope.scopeListPage.size,
                $scope.scopeListPage.sort.property + ',' + $scope.scopeListPage.sort.direction)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scopes = data.content;
                    $scope.scopeListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.getScopes();

    $scope.mouseenter = function (item) {
        item.hover = true;
    };

    $scope.mouseleave = function (item) {
        item.hover = false;
    };

    $scope.$watch('scopeListPage.sort.property', function () {
        if ($scope.scopes) {
            if ($scope.searchParam)
                $scope.searchScopeByName();
            else
                $scope.getScopes();
        }
    });

    $scope.$watch('scopeListPage.number', function () {
        if ($scope.scopes) {
            if ($scope.searchParam)
                $scope.searchScopeByName();
            else
                $scope.getScopes();
        }
    });

    $scope.$watch('scopeListPage.size', function () {
        if ($scope.scopes) {
            if ($scope.searchParam)
                $scope.searchScopeByName();
            else
                $scope.getScopes();
        }
    });

    $scope.searchScopeByName = function () {
        $scope.loading = true;
        SearchService.findScopesByName($scope.scopeListPage, $scope.searchParam)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.scopes = data.content;
                    $scope.scopeListPage.totalPages = data.totalPages;
                    $scope.loading = false;
                } else {
                    $scope.loading = false;
                }
            })
            .error(function (response) {
                $scope.loading = false;
            });
    };

    $scope.$watch('searchParam', function () {
        if ($scope.scopes) {
            $scope.searchScopeByName();
        }
    });

    $scope.previousPage = function () {
        $scope.scopeListPage.number--;
    };

    $scope.nextPage = function () {
        $scope.scopeListPage.number++;
    };
});
